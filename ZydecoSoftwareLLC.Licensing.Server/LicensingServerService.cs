﻿/**
 * LicensingServerService.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;

namespace ZydecoSoftwareLLC.Licensing.Server {
    partial class LicensingServerService: ServiceBase {
        public LicensingServerService() {
            InitializeComponent();
        }

        private bool[] running = new bool[1];

        protected override void OnStart(string[] arguments) {
            Program.RunInternal();
        }

        protected override void OnStop() {
            Environment.Exit(0);
        }
    }
}
