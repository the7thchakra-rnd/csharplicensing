﻿/**
 * Program.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.ComponentModel;
using System.ServiceProcess;
using System.Configuration.Install;
using System.Collections;
using System.Reflection;

namespace ZydecoSoftwareLLC.Licensing.Server {
    static class Program {

        private static TimeSpan notificationInterval = new TimeSpan(5 * TimeSpan.TicksPerSecond);

        private static int port;

        private static ConsoleForm form;

        [STAThread]
        static void Main(string[] arguments) {
            License.DisableListenThread();

            if ((arguments == null) || (arguments.Length == 0)) {
                Run();
            }
            else if ("/console".Equals(arguments[0])) {
                RunConsole();
            }
            else if ("/run".Equals(arguments[0])) {
                RunInternal();
            }
            else if ("/install".Equals(arguments[0])) {
                Install();      
            }
            else if ("/uninstall".Equals(arguments[0])) {
                Uninstall();
            }
            else if ("/purge".Equals(arguments[0])) {
                Purge();
            }
            else if ("/license".Equals(arguments[0])) {
                Assembly current = Assembly.GetExecutingAssembly();
                FileInfo path = new FileInfo(current.Location);
                DirectoryInfo dir = path.Directory;
                String managementAssemblyPath = dir.FullName + @"\" + "ZydecoSoftwareLLC.Licensing.Management.dll";
                Assembly assembly = Assembly.LoadFile(managementAssemblyPath);
                Type type = assembly.GetType("ZydecoSoftwareLLC.Licensing.Management.LicenseManagementServices");
                MethodInfo showCreateLicenseCertificateForm = type.GetMethod("ShowCreateLicenseCertificateForm", new Type[0]);
                showCreateLicenseCertificateForm.Invoke(null, new object[0]);
            }
        }

        private static void Run() {
            ServiceBase.Run(new LicensingServerService());
        }

        internal static void RunInternal() {
            LicensingServices.EnsureIsLicensed(true, false);

            // Create a socket on an ephemeral port:
            StartRequestThread();

            new Thread(RunLicenseServerAvailableMessageThread).Start();
        }

        private static void RunConsole() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            LicensingServices.EnsureIsLicensed(true);

            // Create a socket on an ephemeral port:
            // StartRequestThread();

            // new Thread(RunLicenseServerAvailableMessageThread).Start();

            form = new ConsoleForm();
            form.FormClosed += delegate(object sender, FormClosedEventArgs args) {
                Environment.Exit(0);
            };
            form.TCPEndPoint = (Dns.GetHostName() + ":" + port);

            ThreadPool.QueueUserWorkItem(RefreshGrid);

            Application.Run(form);
        }

        private static void RefreshGrid(object closure) {
            form.RefreshGrid();

            Thread.Sleep(5000);

            ThreadPool.QueueUserWorkItem(RefreshGrid);
        }

        private static void Install() {
            try {
                string[] arguments = new string[] { };
                using (AssemblyInstaller installer = new AssemblyInstaller(typeof(Program).Assembly, arguments)) {
                    IDictionary state = new Hashtable();
                    installer.UseNewContext = true;
                    try {
                        installer.Install(state);
                        installer.Commit(state);
                    }
                    catch {
                        try {
                            installer.Rollback(state);
                        }
                        catch { }
                        throw;
                    }
                }
            }
            catch (Exception ex) {
                Console.Error.WriteLine(ex.Message);
            }
        }

        private static void Uninstall() {
            try {
                string[] arguments = new string[0];
                using (AssemblyInstaller inst = new AssemblyInstaller(typeof(Program).Assembly, arguments)) {
                    IDictionary state = new Hashtable();
                    inst.UseNewContext = true;
                    try {
                        inst.Uninstall(state);
                    }
                    catch {
                        try {
                            inst.Rollback(state);
                        }
                        catch { }
                        throw;
                    }
                }
            }
            catch (Exception ex) {
                Console.Error.WriteLine(ex.Message);
            }
        }

        private static void Purge() {
            License.PurgeLeases();
        }

        private static void StartRequestThread() {
            string tcpAddress;
            IPEndPoint tcp = License.GetLicenseTCPEndPoint(out tcpAddress);
            if (tcp == null) {
                tcp = new IPEndPoint(IPAddress.Any, License.LICENSE_SERVER_TCP_PORT);
            }

            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Bind(tcp);
            socket.Listen(32);

            port = ((IPEndPoint)socket.LocalEndPoint).Port;

            Thread thread = new Thread(RunRequestThread);
            thread.IsBackground = true;
            thread.Start(socket);
        }

        private static void RunRequestThread(object closure) {
            License.Log("Waiting for license requests.");

            Socket socket = (Socket)closure;
            for (; ; ) {
                Socket client = socket.Accept();
                License.Log("Accepted connection from {0}.", client);

                Thread thread = new Thread(ProcessRequest);
                thread.IsBackground = true;
                thread.Start(client);
            }
        }

        private static void ProcessRequest(object request) {
            LicenseRequest lastLicenseRequest = null;

            try {
                using (Socket socket = (Socket)request) {
                    using (NetworkStream stream = new NetworkStream(socket)) {
                        for (; ; ) {
                            object message = ToObject(stream);

                            object response;
                            if (message is LicenseRequest) {
                                lastLicenseRequest = (LicenseRequest)message;
                                response = License.Instance.CreateLease(lastLicenseRequest);
                                if (response == null) {
                                    response = "no leases available";
                                }

                                if (form != null) {
                                    form.RefreshGrid();
                                }
                            }
                            else {
                                response = "invalid";
                            }

                            Serialize(response, stream);
                        }
                    }
                }
            }
            catch (Exception cause) {
                Console.Error.WriteLine(cause);
            }
            finally {
                if (lastLicenseRequest != null) {
                    License.Instance.DropLease(lastLicenseRequest);
                }
            }
        }

        /// <summary>
        /// Periodically report the presence of the license server.
        /// </summary>
        private static void RunLicenseServerAvailableMessageThread() {
            try {
                using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)) {
                    string udpAddress;
                    socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(License.GetLicenseServerMulticastEndPoint(out udpAddress).Address));
                    socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 16);

                    socket.Connect(License.GetLicenseServerMulticastEndPoint(out udpAddress));

                    for (; ; ) {
                        LicenseServerAvailableMessage message = new LicenseServerAvailableMessage();
                        message.Port = port;
                        Send(message, socket);

                        Thread.Sleep(notificationInterval);
                    }
                }
            }
            catch (Exception unhandled) {
                Console.WriteLine(unhandled);
            }
        }

        private static void Send(object message, Socket socket) {
            byte[] b = ToByteArray(message);
            socket.Send(b);
        }

        public static byte[] ToByteArray(object obj) {
            byte[] data;
            using (MemoryStream buffer = new MemoryStream()) {
                Serialize(obj, buffer);
                data = buffer.ToArray();
            }

            return data;
        }

        public static void Serialize(object obj, Stream stream) {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, obj);
        }

        public static object ToObject(byte[] b, int offset, int length) {
            object obj;
            using (MemoryStream buffer = new MemoryStream(b, offset, length)) {
                obj = ToObject(buffer);
            }

            return obj;
        }

        public static object ToObject(Stream stream) {
            object obj;
            BinaryFormatter formatter = new BinaryFormatter();
            obj = formatter.Deserialize(stream);

            return obj;
       }

    }
}
