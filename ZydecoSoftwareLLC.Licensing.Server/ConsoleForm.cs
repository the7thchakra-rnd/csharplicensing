﻿/**
 * ConsoleForm.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace ZydecoSoftwareLLC.Licensing.Server {

    public partial class ConsoleForm : Form {
        private MethodInfo showCreateLicenseCertificateForm;

        public string TCPEndPoint {
            get { return tcpEndPoint.Text; }
            set { tcpEndPoint.Text = value; }
        }

        public ConsoleForm() {
            InitializeComponent();

            try {
                Assembly current = Assembly.GetExecutingAssembly();
                FileInfo path = new FileInfo(current.Location);
                DirectoryInfo dir = path.Directory;
                String managementAssemblyPath = dir.FullName + @"\" + "ZydecoSoftwareLLC.Licensing.Management.dll";
                Assembly assembly = Assembly.LoadFile(managementAssemblyPath);
                Type type = assembly.GetType("ZydecoSoftwareLLC.Licensing.Management.LicenseManagementServices");
                showCreateLicenseCertificateForm = type.GetMethod("ShowCreateLicenseCertificateForm", new Type[0]);
            }
            catch {
                generateLicenseButton.Visible = false;
            }

            licensesView.AutoGenerateColumns = false;

            RefreshGrid();

            leases.Text = License.Instance.LicenseCertificate.MaximumLeases.ToString();
            licenseLeaseDuration.Text = License.Instance.LicenseCertificate.MaximumLicenseLeaseDuration.ToString();

            CenterToScreen();
        }

        private void quitButton_Click(object sender, EventArgs eventArgs) {
            Close();
        }

        private void requestLicenseButton_Click(object sender, EventArgs eventArgs) {
            RequestLicenseForm requestLicenseForm = new RequestLicenseForm();
            requestLicenseForm.Reason = string.Empty;
            requestLicenseForm.ShowDialog();
        }

        private void generateLicenseButton_Click(object sender, EventArgs eventArgs) {
            showCreateLicenseCertificateForm.Invoke(null, new object[0]);
        }

        private delegate void RefreshGridDelegate();

        internal void RefreshGrid() {
            if (InvokeRequired) {
                Invoke(new RefreshGridDelegate(delegate() {
                    RefreshGrid();
                }));
            }
            else {
                licensesView.DataSource = License.Instance.Leases;
            }
        }

        private void licensesView_SelectionChanged(object sender, EventArgs eventArgs) {
            requestedDateTime.Text = string.Empty;
            userName.Text = string.Empty;
            userSID.Text = string.Empty;
            biosSerialNumber.Text = string.Empty;
            cpuID.Text = string.Empty;
            macAddresses.Text = string.Empty;
            version.Text = string.Empty;
            expiresDateTime.Text = string.Empty;

            foreach (DataGridViewRow row in licensesView.SelectedRows) {
                Lease lease = (Lease)row.DataBoundItem;
                LicenseRequest licenseRequest = lease.LicenseRequest;
                LicenseCertificate licenseCertificate = lease.LicenseCertificate;
                requestedDateTime.Text = licenseRequest.LicenseRequestedDateTime.ToString("dddd, MMMM d, yyyy HH:mm ");
                userName.Text = licenseRequest.UserName;
                userSID.Text = licenseRequest.UserSID;
                biosSerialNumber.Text = licenseRequest.BIOSSerialNumber;
                cpuID.Text = licenseRequest.CPUID;
                macAddresses.Text = licenseRequest.MACAddresses;
                version.Text = licenseRequest.Version;
                expiresDateTime.Text = licenseCertificate.ExpiresDateTime.ToString("dddd, MMMM d, yyyy");
            }
        }

    }
}
