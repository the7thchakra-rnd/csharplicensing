﻿namespace ZydecoSoftwareLLC.Licensing.Server {
    partial class ConsoleForm {

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConsoleForm));
            this.tabs = new System.Windows.Forms.TabControl();
            this.licensesTab = new System.Windows.Forms.TabPage();
            this.licenseView = new System.Windows.Forms.GroupBox();
            this.versionLabel = new System.Windows.Forms.Label();
            this.expiresDateTime = new System.Windows.Forms.TextBox();
            this.macAddressesLabel = new System.Windows.Forms.Label();
            this.requestedDateTime = new System.Windows.Forms.TextBox();
            this.version = new System.Windows.Forms.TextBox();
            this.requestedDateTimeLabel = new System.Windows.Forms.Label();
            this.expiresLabel = new System.Windows.Forms.Label();
            this.userName = new System.Windows.Forms.TextBox();
            this.userNameLabel = new System.Windows.Forms.Label();
            this.macAddresses = new System.Windows.Forms.TextBox();
            this.userSIDLabel = new System.Windows.Forms.Label();
            this.cpuID = new System.Windows.Forms.TextBox();
            this.userSID = new System.Windows.Forms.TextBox();
            this.cpuIDLabel = new System.Windows.Forms.Label();
            this.biosSerialNumberLabel = new System.Windows.Forms.Label();
            this.biosSerialNumber = new System.Windows.Forms.TextBox();
            this.licensesView = new System.Windows.Forms.DataGridView();
            this.licensesViewUserNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.licensesViewExpiresColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastPolledColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.settingsPanel = new System.Windows.Forms.GroupBox();
            this.tcpEndPoint = new System.Windows.Forms.TextBox();
            this.tcpEndPointLabel = new System.Windows.Forms.Label();
            this.leases = new System.Windows.Forms.TextBox();
            this.leasesLabel = new System.Windows.Forms.Label();
            this.licenseLeaseDurationUnitsLabel = new System.Windows.Forms.Label();
            this.licenseLeaseDuration = new System.Windows.Forms.TextBox();
            this.licenseLeaseDurationLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.generateLicenseButton = new System.Windows.Forms.Button();
            this.requestLicenseButton = new System.Windows.Forms.Button();
            this.startButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.quitButton = new System.Windows.Forms.Button();
            this.tabs.SuspendLayout();
            this.licensesTab.SuspendLayout();
            this.licenseView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.licensesView)).BeginInit();
            this.settingsPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.licensesTab);
            this.tabs.Location = new System.Drawing.Point(24, 23);
            this.tabs.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(1920, 1352);
            this.tabs.TabIndex = 0;
            // 
            // licensesTab
            // 
            this.licensesTab.BackColor = System.Drawing.SystemColors.Control;
            this.licensesTab.Controls.Add(this.licenseView);
            this.licensesTab.Controls.Add(this.licensesView);
            this.licensesTab.Controls.Add(this.settingsPanel);
            this.licensesTab.Location = new System.Drawing.Point(8, 39);
            this.licensesTab.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.licensesTab.Name = "licensesTab";
            this.licensesTab.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.licensesTab.Size = new System.Drawing.Size(1904, 1305);
            this.licensesTab.TabIndex = 0;
            this.licensesTab.Text = "Licenses";
            // 
            // licenseView
            // 
            this.licenseView.Controls.Add(this.versionLabel);
            this.licenseView.Controls.Add(this.expiresDateTime);
            this.licenseView.Controls.Add(this.macAddressesLabel);
            this.licenseView.Controls.Add(this.requestedDateTime);
            this.licenseView.Controls.Add(this.version);
            this.licenseView.Controls.Add(this.requestedDateTimeLabel);
            this.licenseView.Controls.Add(this.expiresLabel);
            this.licenseView.Controls.Add(this.userName);
            this.licenseView.Controls.Add(this.userNameLabel);
            this.licenseView.Controls.Add(this.macAddresses);
            this.licenseView.Controls.Add(this.userSIDLabel);
            this.licenseView.Controls.Add(this.cpuID);
            this.licenseView.Controls.Add(this.userSID);
            this.licenseView.Controls.Add(this.cpuIDLabel);
            this.licenseView.Controls.Add(this.biosSerialNumberLabel);
            this.licenseView.Controls.Add(this.biosSerialNumber);
            this.licenseView.Location = new System.Drawing.Point(12, 998);
            this.licenseView.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.licenseView.Name = "licenseView";
            this.licenseView.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.licenseView.Size = new System.Drawing.Size(1880, 292);
            this.licenseView.TabIndex = 36;
            this.licenseView.TabStop = false;
            this.licenseView.Text = "License Details";
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.Location = new System.Drawing.Point(12, 213);
            this.versionLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(91, 25);
            this.versionLabel.TabIndex = 46;
            this.versionLabel.Text = "Version:";
            // 
            // expiresDateTime
            // 
            this.expiresDateTime.BackColor = System.Drawing.SystemColors.Control;
            this.expiresDateTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.expiresDateTime.Location = new System.Drawing.Point(112, 250);
            this.expiresDateTime.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.expiresDateTime.Name = "expiresDateTime";
            this.expiresDateTime.ReadOnly = true;
            this.expiresDateTime.Size = new System.Drawing.Size(1756, 24);
            this.expiresDateTime.TabIndex = 45;
            // 
            // macAddressesLabel
            // 
            this.macAddressesLabel.AutoSize = true;
            this.macAddressesLabel.Location = new System.Drawing.Point(12, 177);
            this.macAddressesLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.macAddressesLabel.Name = "macAddressesLabel";
            this.macAddressesLabel.Size = new System.Drawing.Size(173, 25);
            this.macAddressesLabel.TabIndex = 44;
            this.macAddressesLabel.Text = "MAC Addresses:";
            // 
            // requestedDateTime
            // 
            this.requestedDateTime.BackColor = System.Drawing.SystemColors.Control;
            this.requestedDateTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.requestedDateTime.Location = new System.Drawing.Point(200, 31);
            this.requestedDateTime.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.requestedDateTime.Name = "requestedDateTime";
            this.requestedDateTime.ReadOnly = true;
            this.requestedDateTime.Size = new System.Drawing.Size(1668, 24);
            this.requestedDateTime.TabIndex = 43;
            // 
            // version
            // 
            this.version.BackColor = System.Drawing.SystemColors.Control;
            this.version.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.version.Location = new System.Drawing.Point(112, 213);
            this.version.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.version.Name = "version";
            this.version.ReadOnly = true;
            this.version.Size = new System.Drawing.Size(1750, 24);
            this.version.TabIndex = 35;
            // 
            // requestedDateTimeLabel
            // 
            this.requestedDateTimeLabel.AutoSize = true;
            this.requestedDateTimeLabel.Location = new System.Drawing.Point(12, 31);
            this.requestedDateTimeLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.requestedDateTimeLabel.Name = "requestedDateTimeLabel";
            this.requestedDateTimeLabel.Size = new System.Drawing.Size(173, 25);
            this.requestedDateTimeLabel.TabIndex = 28;
            this.requestedDateTimeLabel.Text = "Requested Date:";
            // 
            // expiresLabel
            // 
            this.expiresLabel.AutoSize = true;
            this.expiresLabel.Location = new System.Drawing.Point(12, 250);
            this.expiresLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.expiresLabel.Name = "expiresLabel";
            this.expiresLabel.Size = new System.Drawing.Size(90, 25);
            this.expiresLabel.TabIndex = 32;
            this.expiresLabel.Text = "Expires:";
            // 
            // userName
            // 
            this.userName.BackColor = System.Drawing.SystemColors.Control;
            this.userName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.userName.Location = new System.Drawing.Point(140, 67);
            this.userName.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.userName.Name = "userName";
            this.userName.ReadOnly = true;
            this.userName.Size = new System.Drawing.Size(798, 24);
            this.userName.TabIndex = 18;
            // 
            // userNameLabel
            // 
            this.userNameLabel.AutoSize = true;
            this.userNameLabel.Location = new System.Drawing.Point(12, 67);
            this.userNameLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.userNameLabel.Name = "userNameLabel";
            this.userNameLabel.Size = new System.Drawing.Size(116, 25);
            this.userNameLabel.TabIndex = 17;
            this.userNameLabel.Text = "Username:";
            // 
            // macAddresses
            // 
            this.macAddresses.BackColor = System.Drawing.SystemColors.Control;
            this.macAddresses.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.macAddresses.Location = new System.Drawing.Point(194, 177);
            this.macAddresses.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.macAddresses.Name = "macAddresses";
            this.macAddresses.ReadOnly = true;
            this.macAddresses.Size = new System.Drawing.Size(1674, 24);
            this.macAddresses.TabIndex = 26;
            // 
            // userSIDLabel
            // 
            this.userSIDLabel.AutoSize = true;
            this.userSIDLabel.Location = new System.Drawing.Point(950, 67);
            this.userSIDLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.userSIDLabel.Name = "userSIDLabel";
            this.userSIDLabel.Size = new System.Drawing.Size(103, 25);
            this.userSIDLabel.TabIndex = 20;
            this.userSIDLabel.Text = "User SID:";
            // 
            // cpuID
            // 
            this.cpuID.BackColor = System.Drawing.SystemColors.Control;
            this.cpuID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cpuID.Location = new System.Drawing.Point(116, 140);
            this.cpuID.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.cpuID.Name = "cpuID";
            this.cpuID.ReadOnly = true;
            this.cpuID.Size = new System.Drawing.Size(1752, 24);
            this.cpuID.TabIndex = 25;
            // 
            // userSID
            // 
            this.userSID.BackColor = System.Drawing.SystemColors.Control;
            this.userSID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.userSID.Location = new System.Drawing.Point(1068, 67);
            this.userSID.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.userSID.Name = "userSID";
            this.userSID.ReadOnly = true;
            this.userSID.Size = new System.Drawing.Size(800, 24);
            this.userSID.TabIndex = 21;
            // 
            // cpuIDLabel
            // 
            this.cpuIDLabel.AutoSize = true;
            this.cpuIDLabel.Location = new System.Drawing.Point(12, 140);
            this.cpuIDLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.cpuIDLabel.Name = "cpuIDLabel";
            this.cpuIDLabel.Size = new System.Drawing.Size(88, 25);
            this.cpuIDLabel.TabIndex = 24;
            this.cpuIDLabel.Text = "CPU ID:";
            // 
            // biosSerialNumberLabel
            // 
            this.biosSerialNumberLabel.AutoSize = true;
            this.biosSerialNumberLabel.Location = new System.Drawing.Point(12, 104);
            this.biosSerialNumberLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.biosSerialNumberLabel.Name = "biosSerialNumberLabel";
            this.biosSerialNumberLabel.Size = new System.Drawing.Size(209, 25);
            this.biosSerialNumberLabel.TabIndex = 22;
            this.biosSerialNumberLabel.Text = "BIOS Serial Number:";
            // 
            // biosSerialNumber
            // 
            this.biosSerialNumber.BackColor = System.Drawing.SystemColors.Control;
            this.biosSerialNumber.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.biosSerialNumber.Location = new System.Drawing.Point(232, 104);
            this.biosSerialNumber.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.biosSerialNumber.Name = "biosSerialNumber";
            this.biosSerialNumber.ReadOnly = true;
            this.biosSerialNumber.Size = new System.Drawing.Size(1636, 24);
            this.biosSerialNumber.TabIndex = 23;
            // 
            // licensesView
            // 
            this.licensesView.AllowUserToAddRows = false;
            this.licensesView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.licensesView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.licensesView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.licensesViewUserNameColumn,
            this.licensesViewExpiresColumn,
            this.lastPolledColumn});
            this.licensesView.Location = new System.Drawing.Point(12, 173);
            this.licensesView.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.licensesView.MultiSelect = false;
            this.licensesView.Name = "licensesView";
            this.licensesView.ReadOnly = true;
            this.licensesView.RowHeadersWidth = 82;
            this.licensesView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.licensesView.Size = new System.Drawing.Size(1880, 813);
            this.licensesView.TabIndex = 1;
            this.licensesView.SelectionChanged += new System.EventHandler(this.licensesView_SelectionChanged);
            // 
            // licensesViewUserNameColumn
            // 
            this.licensesViewUserNameColumn.DataPropertyName = "UserName";
            this.licensesViewUserNameColumn.FillWeight = 0.3F;
            this.licensesViewUserNameColumn.HeaderText = "User Name";
            this.licensesViewUserNameColumn.MinimumWidth = 10;
            this.licensesViewUserNameColumn.Name = "licensesViewUserNameColumn";
            this.licensesViewUserNameColumn.ReadOnly = true;
            // 
            // licensesViewExpiresColumn
            // 
            this.licensesViewExpiresColumn.DataPropertyName = "ExpiresDateTime";
            this.licensesViewExpiresColumn.FillWeight = 0.3F;
            this.licensesViewExpiresColumn.HeaderText = "Expires";
            this.licensesViewExpiresColumn.MinimumWidth = 10;
            this.licensesViewExpiresColumn.Name = "licensesViewExpiresColumn";
            this.licensesViewExpiresColumn.ReadOnly = true;
            // 
            // lastPolledColumn
            // 
            this.lastPolledColumn.DataPropertyName = "LastPolled";
            this.lastPolledColumn.FillWeight = 0.3F;
            this.lastPolledColumn.HeaderText = "Last Polled";
            this.lastPolledColumn.MinimumWidth = 10;
            this.lastPolledColumn.Name = "lastPolledColumn";
            this.lastPolledColumn.ReadOnly = true;
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.tcpEndPoint);
            this.settingsPanel.Controls.Add(this.tcpEndPointLabel);
            this.settingsPanel.Controls.Add(this.leases);
            this.settingsPanel.Controls.Add(this.leasesLabel);
            this.settingsPanel.Controls.Add(this.licenseLeaseDurationUnitsLabel);
            this.settingsPanel.Controls.Add(this.licenseLeaseDuration);
            this.settingsPanel.Controls.Add(this.licenseLeaseDurationLabel);
            this.settingsPanel.Location = new System.Drawing.Point(12, 12);
            this.settingsPanel.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.settingsPanel.Name = "settingsPanel";
            this.settingsPanel.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.settingsPanel.Size = new System.Drawing.Size(1880, 150);
            this.settingsPanel.TabIndex = 0;
            this.settingsPanel.TabStop = false;
            this.settingsPanel.Text = "Settings";
            // 
            // tcpEndPoint
            // 
            this.tcpEndPoint.Location = new System.Drawing.Point(1114, 37);
            this.tcpEndPoint.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.tcpEndPoint.Name = "tcpEndPoint";
            this.tcpEndPoint.ReadOnly = true;
            this.tcpEndPoint.Size = new System.Drawing.Size(750, 31);
            this.tcpEndPoint.TabIndex = 6;
            // 
            // tcpEndPointLabel
            // 
            this.tcpEndPointLabel.AutoSize = true;
            this.tcpEndPointLabel.Location = new System.Drawing.Point(938, 42);
            this.tcpEndPointLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.tcpEndPointLabel.Name = "tcpEndPointLabel";
            this.tcpEndPointLabel.Size = new System.Drawing.Size(166, 25);
            this.tcpEndPointLabel.TabIndex = 5;
            this.tcpEndPointLabel.Text = "Server Address:";
            // 
            // leases
            // 
            this.leases.Location = new System.Drawing.Point(264, 87);
            this.leases.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.leases.Name = "leases";
            this.leases.ReadOnly = true;
            this.leases.Size = new System.Drawing.Size(512, 31);
            this.leases.TabIndex = 46;
            this.leases.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // leasesLabel
            // 
            this.leasesLabel.AutoSize = true;
            this.leasesLabel.Location = new System.Drawing.Point(164, 92);
            this.leasesLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.leasesLabel.Name = "leasesLabel";
            this.leasesLabel.Size = new System.Drawing.Size(88, 25);
            this.leasesLabel.TabIndex = 47;
            this.leasesLabel.Text = "Leases:";
            // 
            // licenseLeaseDurationUnitsLabel
            // 
            this.licenseLeaseDurationUnitsLabel.AutoSize = true;
            this.licenseLeaseDurationUnitsLabel.Location = new System.Drawing.Point(792, 42);
            this.licenseLeaseDurationUnitsLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.licenseLeaseDurationUnitsLabel.Name = "licenseLeaseDurationUnitsLabel";
            this.licenseLeaseDurationUnitsLabel.Size = new System.Drawing.Size(87, 25);
            this.licenseLeaseDurationUnitsLabel.TabIndex = 2;
            this.licenseLeaseDurationUnitsLabel.Text = "minutes";
            // 
            // licenseLeaseDuration
            // 
            this.licenseLeaseDuration.Location = new System.Drawing.Point(264, 37);
            this.licenseLeaseDuration.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.licenseLeaseDuration.Name = "licenseLeaseDuration";
            this.licenseLeaseDuration.ReadOnly = true;
            this.licenseLeaseDuration.Size = new System.Drawing.Size(512, 31);
            this.licenseLeaseDuration.TabIndex = 1;
            this.licenseLeaseDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // licenseLeaseDurationLabel
            // 
            this.licenseLeaseDurationLabel.AutoSize = true;
            this.licenseLeaseDurationLabel.Location = new System.Drawing.Point(12, 42);
            this.licenseLeaseDurationLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.licenseLeaseDurationLabel.Name = "licenseLeaseDurationLabel";
            this.licenseLeaseDurationLabel.Size = new System.Drawing.Size(245, 25);
            this.licenseLeaseDurationLabel.TabIndex = 0;
            this.licenseLeaseDurationLabel.Text = "License Lease Duration:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.generateLicenseButton);
            this.panel1.Controls.Add(this.requestLicenseButton);
            this.panel1.Controls.Add(this.startButton);
            this.panel1.Controls.Add(this.stopButton);
            this.panel1.Controls.Add(this.quitButton);
            this.panel1.Location = new System.Drawing.Point(24, 1387);
            this.panel1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1920, 56);
            this.panel1.TabIndex = 1;
            // 
            // generateLicenseButton
            // 
            this.generateLicenseButton.Location = new System.Drawing.Point(284, 6);
            this.generateLicenseButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.generateLicenseButton.Name = "generateLicenseButton";
            this.generateLicenseButton.Size = new System.Drawing.Size(264, 44);
            this.generateLicenseButton.TabIndex = 6;
            this.generateLicenseButton.Text = "&Generate License";
            this.generateLicenseButton.UseVisualStyleBackColor = true;
            this.generateLicenseButton.Click += new System.EventHandler(this.generateLicenseButton_Click);
            // 
            // requestLicenseButton
            // 
            this.requestLicenseButton.Location = new System.Drawing.Point(8, 6);
            this.requestLicenseButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.requestLicenseButton.Name = "requestLicenseButton";
            this.requestLicenseButton.Size = new System.Drawing.Size(264, 44);
            this.requestLicenseButton.TabIndex = 5;
            this.requestLicenseButton.Text = "&Request License";
            this.requestLicenseButton.UseVisualStyleBackColor = true;
            this.requestLicenseButton.Click += new System.EventHandler(this.requestLicenseButton_Click);
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(1438, 6);
            this.startButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(150, 44);
            this.startButton.TabIndex = 4;
            this.startButton.Text = "S&tart";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Visible = false;
            // 
            // stopButton
            // 
            this.stopButton.Location = new System.Drawing.Point(1600, 6);
            this.stopButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(150, 44);
            this.stopButton.TabIndex = 3;
            this.stopButton.Text = "S&top";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Visible = false;
            // 
            // quitButton
            // 
            this.quitButton.Location = new System.Drawing.Point(1762, 6);
            this.quitButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.quitButton.Name = "quitButton";
            this.quitButton.Size = new System.Drawing.Size(150, 44);
            this.quitButton.TabIndex = 2;
            this.quitButton.Text = "&Quit";
            this.quitButton.UseVisualStyleBackColor = true;
            this.quitButton.Click += new System.EventHandler(this.quitButton_Click);
            // 
            // ConsoleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1968, 1465);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabs);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "ConsoleForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "ZydecoSoftwareLLC, LLC :: License Server";
            this.tabs.ResumeLayout(false);
            this.licensesTab.ResumeLayout(false);
            this.licenseView.ResumeLayout(false);
            this.licenseView.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.licensesView)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage licensesTab;
        private System.Windows.Forms.GroupBox settingsPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Button quitButton;
        private System.Windows.Forms.DataGridView licensesView;
        private System.Windows.Forms.DataGridViewTextBoxColumn licensesViewExpiresColumn;
        private System.Windows.Forms.Button requestLicenseButton;
        private System.Windows.Forms.Button generateLicenseButton;
        private System.Windows.Forms.Label licenseLeaseDurationUnitsLabel;
        private System.Windows.Forms.TextBox licenseLeaseDuration;
        private System.Windows.Forms.Label licenseLeaseDurationLabel;
        private System.Windows.Forms.TextBox leases;
        private System.Windows.Forms.Label leasesLabel;
        private System.Windows.Forms.GroupBox licenseView;
        private System.Windows.Forms.DataGridViewTextBoxColumn licensesViewUserNameColumn;
        private System.Windows.Forms.TextBox tcpEndPoint;
        private System.Windows.Forms.Label tcpEndPointLabel;
        private System.Windows.Forms.Label userNameLabel;
        private System.Windows.Forms.TextBox userName;
        private System.Windows.Forms.Label userSIDLabel;
        private System.Windows.Forms.TextBox userSID;
        private System.Windows.Forms.Label  biosSerialNumberLabel;
        private System.Windows.Forms.TextBox biosSerialNumber;
        private System.Windows.Forms.Label cpuIDLabel;
        private System.Windows.Forms.TextBox cpuID;
        private System.Windows.Forms.TextBox macAddresses;
        private System.Windows.Forms.Label requestedDateTimeLabel;
        private System.Windows.Forms.Label expiresLabel;
        private System.Windows.Forms.TextBox version;
        private System.Windows.Forms.TextBox requestedDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastPolledColumn;
        private System.Windows.Forms.Label macAddressesLabel;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.TextBox expiresDateTime;

    }
}

