﻿/**
 * LicenseManagementServices.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using System;
using System.IO;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;

namespace ZydecoSoftwareLLC.Licensing.Management {
    public class LicenseManagementServices {
        public static void ShowCreateLicenseCertificateForm() {
            CreateLicenseCertificateForm createLicenseCertificateForm = new CreateLicenseCertificateForm();
            createLicenseCertificateForm.ShowDialog();
        }

        public void ShowCreateLicenseCertificateForm(out string licenseRequest, out string licenseRequestDetails, out string licenseCertificate, out string licenseCertificateDetails) {
            CreateLicenseCertificateForm createLicenseCertificateForm = new CreateLicenseCertificateForm();
            createLicenseCertificateForm.ShowDialog();
            licenseRequest = createLicenseCertificateForm.LicenseRequest;
            licenseRequestDetails = createLicenseCertificateForm.LicenseRequestDetails;
            licenseCertificate = createLicenseCertificateForm.LicenseCertificate;
            licenseCertificateDetails = createLicenseCertificateForm.LicenseCertificateDetails;
        }

        /// <summary>
        /// Try to load a license request from the given text.
        /// </summary>
        /// <param name="text">the text</param>
        /// <returns>a license request from the given text</returns>
        public static LicenseRequest LoadLicenseRequest(byte[] password, string text) {
            LicenseRequest request;
            TryGetRequestObject<LicenseRequest>(password, "LicenseRequest", text, out request);

            return request;
        }
        /// <summary>
        /// Try to extract a request object as a token (base-64 encoded byte sequence
        /// embedded in XML) from the given text.
        /// </summary>
        /// <param name="password">the private key password</param>
        /// <param name="marker">the token marker (tag name)</param>
        /// <param name="text">the text</param>
        /// <param name="requestObject">the request object</param>
        public static bool TryGetRequestObject<T>(byte[] password, string marker, string text, out T requestObject) {
            bool found;
            string token;
            found = TryGetRequestObject<T>(password, marker, text, out token, out requestObject);

            return found;
        }

        public static bool TryGetRequestObject<T>(byte[] password, string marker, string text, out string matched, out T requestObject) {
            bool found;

            string token = CryptographyServices.GetToken(marker, text, out matched);
            if (token == null) {
                found = false;

                requestObject = default(T);
            }
            else {
                found = true;

                byte[] encrypted = Convert.FromBase64String(token);
                byte[] bytes = PrivateKeyDecrypt(password, encrypted);
                requestObject = (T)CryptographyServices.ToObject<T>(bytes);
            }

            return found;
        }

        internal static string ToResponseToken(object obj) {
            string value;
            using (MemoryStream buffer = new MemoryStream()) {
                // TODO:
                // using (CryptoStream encryptor = GetEncryptStream(buffer, CryptoStreamMode.Write)) {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.AssemblyFormat = FormatterAssemblyStyle.Simple;
                formatter.Serialize(buffer, obj);
                // }

                byte[] bytes = buffer.ToArray();
                value = Convert.ToBase64String(bytes);
            }

            return value;
        }

        public static byte[] PrivateKeyDecrypt(byte[] password, byte[] encrypted) {
            byte[] bytes;

            using (PrivateKeyDecrypt decrypt = new PrivateKeyDecrypt(password)) {
                bytes = decrypt.Decrypt(encrypted);
            }

            return bytes;
        }
    }
}
