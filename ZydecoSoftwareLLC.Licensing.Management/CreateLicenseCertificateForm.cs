﻿/**
 * CreateLicenseCertificateForm.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace ZydecoSoftwareLLC.Licensing.Management {
    public partial class CreateLicenseCertificateForm: ClipboardAwareForm {
        private const string REGISTRYKEY_ROOT = "Software\\ZydecoSoftwareLLC, LLC";

        private bool initialized;
        private LicenseRequest licenseRequest;
        private string licenseRequestText;
        private LicenseCertificate licenseCertificate;
        private string licenseCertificateText;

        /// <summary>
        /// Constructor.
        /// </summary>
        public CreateLicenseCertificateForm() {
            InitializeComponent();
            initialized = true;

            CenterToScreen();

            LoadSettings();

            DrawClipboard();
        }

        protected override void DrawClipboard() {
            if (initialized) {
                string text = Clipboard.GetText();

                // Try to get the license request from the clipboard:
                LicenseRequest licenseRequest;
                string licenseRequestText;
                bool changed;
                try {
                    string systemPasswordText = systemPassword.Text;
                    byte[] systemPasswordBytes = Encoding.UTF8.GetBytes(systemPasswordText);
                    LicenseManagementServices.TryGetRequestObject<LicenseRequest>(systemPasswordBytes, "LicenseRequest", text, out licenseRequestText, out licenseRequest);
                    changed = ((licenseRequest != null) && !string.Equals(this.licenseRequestText, licenseRequestText));
                }
                catch {
                    licenseRequest = null;
                    licenseRequestText = null;
                    changed = false;
                }

                if (licenseRequest == null) {
                    if (this.licenseRequest == null) {
                        clipboardContents.Text = text;

                        this.licenseRequest = null;
                        this.licenseRequestText = null;
                    }
                }
                else if (changed) {
                    clipboardContents.Text = licenseRequestText;

                    this.licenseRequest = licenseRequest;
                    this.licenseRequestText = licenseRequestText;

                    licenseRequestPanel.Visible = true;

                    copyLicenseCertificateToClipboard.Enabled = true;

                    DateTime licenseRequestedDateTimeValue = licenseRequest.LicenseRequestedDateTime;
                    licenseRequestedDateTime.Text = licenseRequestedDateTimeValue.ToString("dddd, MMMM d, yyyy");

                    Guid licenseRequestIDValue = licenseRequest.LicenseRequestID;
                    licenseRequestID.Text = Convert.ToString(licenseRequestIDValue);

                    string userNameText = licenseRequest.UserName;
                    userName.Text = userNameText;
                    userNameLocked.Checked = !string.IsNullOrWhiteSpace(userNameText);
                    userNameLocked.Enabled = !string.IsNullOrWhiteSpace(userNameText);

                    string userSIDText = licenseRequest.UserSID;
                    userSID.Text = userSIDText;
                    userSIDLocked.Checked = false;
                    userSIDLocked.Enabled = !string.IsNullOrWhiteSpace(userSIDText);

                    string biosSerialNumberText = licenseRequest.BIOSSerialNumber;
                    biosSerialNumber.Text = biosSerialNumberText;
                    biosSerialNumberLocked.Checked = !string.IsNullOrWhiteSpace(biosSerialNumberText);
                    biosSerialNumberLocked.Enabled = !string.IsNullOrWhiteSpace(biosSerialNumberText);

                    string cpuIDText = licenseRequest.CPUID;
                    cpuID.Text = cpuIDText;
                    cpuIDLocked.Checked = !string.IsNullOrWhiteSpace(cpuIDText);
                    cpuIDLocked.Enabled = !string.IsNullOrWhiteSpace(cpuIDText);
                    cpuID.Text = licenseRequest.CPUID;

                    string macAddressesText = licenseRequest.MACAddresses;
                    macAddresses.Text = macAddressesText;
                    macAddressesLocked.Checked = !string.IsNullOrWhiteSpace(macAddressesText);
                    macAddressesLocked.Enabled = !string.IsNullOrWhiteSpace(macAddressesText);

                    expires.Checked = true;
                    DateTime expiresDateTimeValue = new DateTime((long)(DateTime.UtcNow.Ticks / TimeSpan.TicksPerDay) * TimeSpan.TicksPerDay).AddMonths(6);
                    expiresDateTimeValue = expiresDateTimeValue.AddDays((DayOfWeek.Monday - expiresDateTimeValue.DayOfWeek + 7) % 7);
                    expiresDateTime.Value = expiresDateTimeValue;

                    string versionText = licenseRequest.Version;
                    string[] versionParts = ((versionText == null)? null: versionText.Split('.'));
                    if ((versionParts == null) || (versionParts.Length < 1) || string.IsNullOrWhiteSpace(versionParts[0])) {
                        majorVersionLocked.Enabled = false;
                        majorVersionLocked.Checked = false;
                        majorVersion.Text = string.Empty;
                    }
                    else {
                        majorVersionLocked.Enabled = true;
                        majorVersionLocked.Checked = true;
                        majorVersion.Text = versionParts[0];
                    }
                    if ((versionParts == null) || (versionParts.Length < 2) || string.IsNullOrWhiteSpace(versionParts[1])) {
                        minorVersionLocked.Enabled = false;
                        minorVersionLocked.Checked = false;
                        minorVersion.Text = string.Empty;
                    }
                    else {
                        minorVersionLocked.Enabled = true;
                        minorVersionLocked.Checked = true;
                        minorVersion.Text = versionParts[1];
                    }
                    if ((versionParts == null) || (versionParts.Length < 3) || string.IsNullOrWhiteSpace(versionParts[2])) {
                        revisionLocked.Enabled = false;
                        revisionLocked.Checked = false;
                        revision.Text = string.Empty;
                    }
                    else {
                        revisionLocked.Enabled = true;
                        revisionLocked.Checked = true;
                        revision.Text = versionParts[2];
                    }
                    if ((versionParts == null) || (versionParts.Length < 4) || string.IsNullOrWhiteSpace(versionParts[3])) {
                        buildLocked.Enabled = false;
                        buildLocked.Checked = false;
                        build.Text = string.Empty;
                    }
                    else {
                        buildLocked.Enabled = true;
                        buildLocked.Checked = false;
                        build.Text = versionParts[3];
                    }
                }

                licenseRequestPanel.Visible = (this.licenseRequest != null);

                copyLicenseCertificateToClipboard.Enabled = (this.licenseRequest != null);
            }
        }

        private void systemPassword_TextChanged(object sender, EventArgs eventArgs) {
            SaveSettings();
            
            DrawClipboard();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs eventArgs) {
            if (licenseRequest != null) {
                licenseCertificate = new LicenseCertificate(licenseRequest);
                licenseCertificate.IsUserNamedLocked = userNameLocked.Checked;
                licenseCertificate.IsUserSIDLocked = userSIDLocked.Checked;
                licenseCertificate.IsBIOSSerialNumberLocked = biosSerialNumberLocked.Checked;
                licenseCertificate.IsCPUIDLocked = cpuIDLocked.Checked;
                licenseCertificate.IsMACAddressesLocked = macAddressesLocked.Checked;
                licenseCertificate.IsExpiring = expires.Checked;
                licenseCertificate.ExpiresDateTime = expiresDateTime.Value;
                licenseCertificate.IsMajorVersionLocked = majorVersionLocked.Checked;
                licenseCertificate.IsMinorVersionLocked = minorVersionLocked.Checked;
                licenseCertificate.IsRevisionLocked = revisionLocked.Checked;
                licenseCertificate.IsBuildLocked = buildLocked.Checked;
                licenseCertificate.IsServerCertificate = serverCertificate.Checked;

                int leases;
                int.TryParse(this.leases.Text, out leases);
                licenseCertificate.MaximumLeases = leases;

                int leaseDuration;
                int.TryParse(this.leaseDuration.Text, out leaseDuration);
                licenseCertificate.MaximumLicenseLeaseDuration = leaseDuration;

                licenseCertificateText = licenseRequest.GetJacketResponseToken("LicenseCertificate", licenseCertificate);

                Clipboard.SetText(licenseCertificateText);
            }
        }

        private void showSystemPassword_CheckedChanged(object sender, EventArgs e) {
            systemPassword.PasswordChar = (showSystemPassword.Checked ? (char)0 : '*');
        }

        private void rememberSystemPassword_CheckedChanged(object sender, EventArgs e) {
            SaveSettings();
        }

        /// <summary>
        /// Load saved settings.
        /// </summary>
        private void LoadSettings() {
            RegistryKey root = Registry.CurrentUser.OpenSubKey(REGISTRYKEY_ROOT, true);
            if (root != null) {
                using (root) {
                    Dictionary<string, object> settings;
                    string token = (root.GetValue("Settings") as string);
                    settings = CryptographyServices.GetFromToken<Dictionary<string, object>>(token);

                    if (settings != null) {
                        object rememberSystemPasswordChecked;
                        if (settings.TryGetValue("rememberSystemPassword", out rememberSystemPasswordChecked) && (rememberSystemPasswordChecked is bool)) {
                            rememberSystemPassword.Checked = (bool)rememberSystemPasswordChecked;
                        }

                        if (rememberSystemPassword.Checked) {
                            object systemPasswordText;
                            if (settings.TryGetValue("systemPassword", out systemPasswordText) && (systemPasswordText is string)) {
                                systemPassword.Text = (string)systemPasswordText;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Save settings.
        /// </summary>
        private void SaveSettings() {
            RegistryKey root = Registry.CurrentUser.OpenSubKey(REGISTRYKEY_ROOT, true);
            if (root == null) {
                root = Registry.CurrentUser.CreateSubKey(REGISTRYKEY_ROOT);
            }

            using (root) {
                Dictionary<string, object> settings = new Dictionary<string, object>();

                bool rememberSystemPasswordChecked = rememberSystemPassword.Checked;
                settings["rememberSystemPassword"] = rememberSystemPasswordChecked;
                if (rememberSystemPasswordChecked) {
                    settings["systemPassword"] = systemPassword.Text;
                }

                string token = CryptographyServices.ToToken(settings);
                root.SetValue("Settings", token);
            }
        }

        private void ok_Click(object sender, EventArgs eventArgs) {
            Close();
        }

        private void expiresDateTime_ValueChanged(object sender, EventArgs eventArgs) {
            string detailsText;
            StringBuilder detailsBuilder = new StringBuilder();
            DateTime now = DateTime.UtcNow;
            TimeSpan span = (expiresDateTime.Value - now);
            if (span.Ticks < 0) {
                detailsBuilder.Append("Expired");
            }
            else {
                string separator = "Expires in ";
                int days = (int)(span.Ticks / TimeSpan.TicksPerDay);
                if (days > 0) {
                    detailsBuilder.Append(string.Format("{1}{0} days", days, separator));
                    separator = ", ";
                }

                /*
                int hours = span.Hours;
                if (hours > 0) {
                    detailsBuilder.Append(string.Format("{1}{0} hours", hours, separator));
                    separator = ", ";
                }

                int minutes = span.Minutes;
                if (minutes > 0) {
                    detailsBuilder.Append(string.Format("{1}{0} minutes", minutes, separator));
                    separator = ", ";
                }

                int seconds = span.Seconds;
                if (seconds > 0) {
                    detailsBuilder.Append(string.Format("{1}{0} seconds", seconds, separator));
                    separator = ", ";
                }
                */
            }
            detailsText = detailsBuilder.ToString();

            expiresDetails.Text = detailsText;
        }

        public string LicenseRequest {
            get {
                string value;
                if ((licenseRequest == null) || string.IsNullOrWhiteSpace(licenseRequestText)) {
                    value = string.Empty;
                }
                else {
                    value = licenseRequestText;
                }

                return value;
            }
        }

        public string LicenseRequestDetails {
            get {
                string value;
                if (licenseRequest == null) {
                    value = string.Empty;
                }
                else {
                    value = licenseRequest.ToString();
                }

                return value;
            }
        }

        public string LicenseCertificate {
            get {
                string value;
                if ((licenseCertificate == null) || string.IsNullOrWhiteSpace(licenseCertificateText)) {
                    value = string.Empty;
                }
                else {
                    value = licenseCertificateText;
                }

                return value;
            }
        }

        public string LicenseCertificateDetails {
            get {
                string value;
                if (licenseCertificate == null) {
                    value = string.Empty;
                }
                else {
                    value = licenseCertificate.ToString();
                }

                return value;
            }
        }

    }
}
