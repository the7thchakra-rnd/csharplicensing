﻿namespace ZydecoSoftwareLLC.Licensing.Management {
    partial class CreateLicenseCertificateForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateLicenseCertificateForm));
            this.step0 = new System.Windows.Forms.Label();
            this.step1a = new System.Windows.Forms.Label();
            this.clipboardContents = new System.Windows.Forms.TextBox();
            this.systemPassword = new System.Windows.Forms.TextBox();
            this.step2a = new System.Windows.Forms.Label();
            this.step3a = new System.Windows.Forms.Label();
            this.step4 = new System.Windows.Forms.Label();
            this.step4a = new System.Windows.Forms.Label();
            this.copyLicenseCertificateToClipboard = new System.Windows.Forms.LinkLabel();
            this.ok = new System.Windows.Forms.Button();
            this.step1 = new System.Windows.Forms.Label();
            this.step2 = new System.Windows.Forms.Label();
            this.step3 = new System.Windows.Forms.Label();
            this.showSystemPassword = new System.Windows.Forms.CheckBox();
            this.rememberSystemPassword = new System.Windows.Forms.CheckBox();
            this.userNameLocked = new System.Windows.Forms.CheckBox();
            this.userName = new System.Windows.Forms.TextBox();
            this.userSIDLocked = new System.Windows.Forms.CheckBox();
            this.userSID = new System.Windows.Forms.TextBox();
            this.biosSerialNumberLocked = new System.Windows.Forms.CheckBox();
            this.biosSerialNumber = new System.Windows.Forms.TextBox();
            this.cpuIDLocked = new System.Windows.Forms.CheckBox();
            this.cpuID = new System.Windows.Forms.TextBox();
            this.macAddresses = new System.Windows.Forms.TextBox();
            this.macAddressesLocked = new System.Windows.Forms.CheckBox();
            this.requestedDateTimeLabel = new System.Windows.Forms.Label();
            this.licenseRequestID = new System.Windows.Forms.TextBox();
            this.licenseRequestIDLabel = new System.Windows.Forms.Label();
            this.expires = new System.Windows.Forms.CheckBox();
            this.expiresDateTime = new System.Windows.Forms.DateTimePicker();
            this.majorVersionLocked = new System.Windows.Forms.CheckBox();
            this.majorVersion = new System.Windows.Forms.TextBox();
            this.licenseRequestPanel = new System.Windows.Forms.Panel();
            this.leaseDurationUnitsLabel = new System.Windows.Forms.Label();
            this.leaseDuration = new System.Windows.Forms.TextBox();
            this.leaseDurationLabel = new System.Windows.Forms.Label();
            this.leasesLabel = new System.Windows.Forms.Label();
            this.leases = new System.Windows.Forms.TextBox();
            this.serverCertificate = new System.Windows.Forms.CheckBox();
            this.expiresDetails = new System.Windows.Forms.TextBox();
            this.licenseRequestedDateTime = new System.Windows.Forms.TextBox();
            this.build = new System.Windows.Forms.TextBox();
            this.buildLocked = new System.Windows.Forms.CheckBox();
            this.revisionLocked = new System.Windows.Forms.CheckBox();
            this.revision = new System.Windows.Forms.TextBox();
            this.minorVersionLocked = new System.Windows.Forms.CheckBox();
            this.minorVersion = new System.Windows.Forms.TextBox();
            this.licenseRequestPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // step0
            // 
            this.step0.AutoSize = true;
            this.step0.Location = new System.Drawing.Point(26, 25);
            this.step0.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step0.Name = "step0";
            this.step0.Size = new System.Drawing.Size(379, 25);
            this.step0.TabIndex = 0;
            this.step0.Text = "Follow these steps to create a license:";
            // 
            // step1a
            // 
            this.step1a.AutoSize = true;
            this.step1a.Location = new System.Drawing.Point(70, 62);
            this.step1a.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step1a.Name = "step1a";
            this.step1a.Size = new System.Drawing.Size(277, 25);
            this.step1a.TabIndex = 1;
            this.step1a.Text = "Enter the system password:";
            // 
            // clipboardContents
            // 
            this.clipboardContents.BackColor = System.Drawing.SystemColors.Control;
            this.clipboardContents.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.clipboardContents.Location = new System.Drawing.Point(102, 212);
            this.clipboardContents.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.clipboardContents.Multiline = true;
            this.clipboardContents.Name = "clipboardContents";
            this.clipboardContents.ReadOnly = true;
            this.clipboardContents.Size = new System.Drawing.Size(1442, 338);
            this.clipboardContents.TabIndex = 2;
            // 
            // systemPassword
            // 
            this.systemPassword.Location = new System.Drawing.Point(354, 56);
            this.systemPassword.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.systemPassword.Name = "systemPassword";
            this.systemPassword.PasswordChar = '*';
            this.systemPassword.Size = new System.Drawing.Size(1186, 31);
            this.systemPassword.TabIndex = 1;
            this.systemPassword.TextChanged += new System.EventHandler(this.systemPassword_TextChanged);
            // 
            // step2a
            // 
            this.step2a.AutoSize = true;
            this.step2a.Location = new System.Drawing.Point(70, 175);
            this.step2a.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step2a.Name = "step2a";
            this.step2a.Size = new System.Drawing.Size(491, 25);
            this.step2a.TabIndex = 4;
            this.step2a.Text = "Copy the license request block onto the clipboard.";
            // 
            // step3a
            // 
            this.step3a.AutoSize = true;
            this.step3a.Location = new System.Drawing.Point(76, 575);
            this.step3a.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step3a.Name = "step3a";
            this.step3a.Size = new System.Drawing.Size(523, 25);
            this.step3a.TabIndex = 5;
            this.step3a.Text = "Review the license request and configure the license:";
            // 
            // step4
            // 
            this.step4.AutoSize = true;
            this.step4.Location = new System.Drawing.Point(32, 1033);
            this.step4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step4.Name = "step4";
            this.step4.Size = new System.Drawing.Size(31, 25);
            this.step4.TabIndex = 7;
            this.step4.Text = "4)";
            // 
            // step4a
            // 
            this.step4a.AutoSize = true;
            this.step4a.Location = new System.Drawing.Point(410, 1033);
            this.step4a.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step4a.Name = "step4a";
            this.step4a.Size = new System.Drawing.Size(289, 25);
            this.step4a.TabIndex = 8;
            this.step4a.Text = "and e-mail it to the customer.";
            // 
            // copyLicenseCertificateToClipboard
            // 
            this.copyLicenseCertificateToClipboard.AutoSize = true;
            this.copyLicenseCertificateToClipboard.Location = new System.Drawing.Point(76, 1033);
            this.copyLicenseCertificateToClipboard.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.copyLicenseCertificateToClipboard.Name = "copyLicenseCertificateToClipboard";
            this.copyLicenseCertificateToClipboard.Size = new System.Drawing.Size(326, 25);
            this.copyLicenseCertificateToClipboard.TabIndex = 9;
            this.copyLicenseCertificateToClipboard.TabStop = true;
            this.copyLicenseCertificateToClipboard.Text = "Copy the license to the clipboard";
            this.copyLicenseCertificateToClipboard.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // ok
            // 
            this.ok.Location = new System.Drawing.Point(1394, 1077);
            this.ok.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(150, 44);
            this.ok.TabIndex = 10;
            this.ok.Text = "&OK";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // step1
            // 
            this.step1.AutoSize = true;
            this.step1.Location = new System.Drawing.Point(26, 62);
            this.step1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step1.Name = "step1";
            this.step1.Size = new System.Drawing.Size(31, 25);
            this.step1.TabIndex = 11;
            this.step1.Text = "1)";
            // 
            // step2
            // 
            this.step2.AutoSize = true;
            this.step2.Location = new System.Drawing.Point(26, 175);
            this.step2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step2.Name = "step2";
            this.step2.Size = new System.Drawing.Size(31, 25);
            this.step2.TabIndex = 12;
            this.step2.Text = "2)";
            // 
            // step3
            // 
            this.step3.AutoSize = true;
            this.step3.Location = new System.Drawing.Point(32, 575);
            this.step3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step3.Name = "step3";
            this.step3.Size = new System.Drawing.Size(31, 25);
            this.step3.TabIndex = 13;
            this.step3.Text = "3)";
            // 
            // showSystemPassword
            // 
            this.showSystemPassword.AutoSize = true;
            this.showSystemPassword.Location = new System.Drawing.Point(702, 106);
            this.showSystemPassword.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.showSystemPassword.Name = "showSystemPassword";
            this.showSystemPassword.Size = new System.Drawing.Size(197, 29);
            this.showSystemPassword.TabIndex = 15;
            this.showSystemPassword.Text = "Show Password";
            this.showSystemPassword.UseVisualStyleBackColor = true;
            this.showSystemPassword.CheckedChanged += new System.EventHandler(this.showSystemPassword_CheckedChanged);
            // 
            // rememberSystemPassword
            // 
            this.rememberSystemPassword.AutoSize = true;
            this.rememberSystemPassword.Location = new System.Drawing.Point(352, 106);
            this.rememberSystemPassword.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.rememberSystemPassword.Name = "rememberSystemPassword";
            this.rememberSystemPassword.Size = new System.Drawing.Size(325, 29);
            this.rememberSystemPassword.TabIndex = 16;
            this.rememberSystemPassword.Text = "Remember System Password";
            this.rememberSystemPassword.UseVisualStyleBackColor = true;
            this.rememberSystemPassword.CheckedChanged += new System.EventHandler(this.rememberSystemPassword_CheckedChanged);
            // 
            // userNameLocked
            // 
            this.userNameLocked.AutoSize = true;
            this.userNameLocked.Location = new System.Drawing.Point(6, 60);
            this.userNameLocked.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.userNameLocked.Name = "userNameLocked";
            this.userNameLocked.Size = new System.Drawing.Size(148, 29);
            this.userNameLocked.TabIndex = 17;
            this.userNameLocked.Text = "Username:";
            this.userNameLocked.UseVisualStyleBackColor = true;
            // 
            // userName
            // 
            this.userName.BackColor = System.Drawing.SystemColors.Control;
            this.userName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.userName.Location = new System.Drawing.Point(172, 62);
            this.userName.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.userName.Name = "userName";
            this.userName.ReadOnly = true;
            this.userName.Size = new System.Drawing.Size(534, 24);
            this.userName.TabIndex = 18;
            // 
            // userSIDLocked
            // 
            this.userSIDLocked.AutoSize = true;
            this.userSIDLocked.Location = new System.Drawing.Point(718, 60);
            this.userSIDLocked.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.userSIDLocked.Name = "userSIDLocked";
            this.userSIDLocked.Size = new System.Drawing.Size(135, 29);
            this.userSIDLocked.TabIndex = 20;
            this.userSIDLocked.Text = "User SID:";
            this.userSIDLocked.UseVisualStyleBackColor = true;
            // 
            // userSID
            // 
            this.userSID.BackColor = System.Drawing.SystemColors.Control;
            this.userSID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.userSID.Location = new System.Drawing.Point(874, 62);
            this.userSID.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.userSID.Name = "userSID";
            this.userSID.ReadOnly = true;
            this.userSID.Size = new System.Drawing.Size(588, 24);
            this.userSID.TabIndex = 21;
            // 
            // biosSerialNumberLocked
            // 
            this.biosSerialNumberLocked.AutoSize = true;
            this.biosSerialNumberLocked.Location = new System.Drawing.Point(6, 110);
            this.biosSerialNumberLocked.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.biosSerialNumberLocked.Name = "biosSerialNumberLocked";
            this.biosSerialNumberLocked.Size = new System.Drawing.Size(241, 29);
            this.biosSerialNumberLocked.TabIndex = 22;
            this.biosSerialNumberLocked.Text = "BIOS Serial Number:";
            this.biosSerialNumberLocked.UseVisualStyleBackColor = true;
            // 
            // biosSerialNumber
            // 
            this.biosSerialNumber.BackColor = System.Drawing.SystemColors.Control;
            this.biosSerialNumber.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.biosSerialNumber.Location = new System.Drawing.Point(264, 112);
            this.biosSerialNumber.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.biosSerialNumber.Name = "biosSerialNumber";
            this.biosSerialNumber.ReadOnly = true;
            this.biosSerialNumber.Size = new System.Drawing.Size(1198, 24);
            this.biosSerialNumber.TabIndex = 23;
            // 
            // cpuIDLocked
            // 
            this.cpuIDLocked.AutoSize = true;
            this.cpuIDLocked.Location = new System.Drawing.Point(6, 160);
            this.cpuIDLocked.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.cpuIDLocked.Name = "cpuIDLocked";
            this.cpuIDLocked.Size = new System.Drawing.Size(120, 29);
            this.cpuIDLocked.TabIndex = 24;
            this.cpuIDLocked.Text = "CPU ID:";
            this.cpuIDLocked.UseVisualStyleBackColor = true;
            // 
            // cpuID
            // 
            this.cpuID.BackColor = System.Drawing.SystemColors.Control;
            this.cpuID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cpuID.Location = new System.Drawing.Point(148, 167);
            this.cpuID.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.cpuID.Name = "cpuID";
            this.cpuID.ReadOnly = true;
            this.cpuID.Size = new System.Drawing.Size(1314, 24);
            this.cpuID.TabIndex = 25;
            // 
            // macAddresses
            // 
            this.macAddresses.BackColor = System.Drawing.SystemColors.Control;
            this.macAddresses.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.macAddresses.Location = new System.Drawing.Point(226, 212);
            this.macAddresses.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.macAddresses.Name = "macAddresses";
            this.macAddresses.ReadOnly = true;
            this.macAddresses.Size = new System.Drawing.Size(1236, 24);
            this.macAddresses.TabIndex = 26;
            // 
            // macAddressesLocked
            // 
            this.macAddressesLocked.AutoSize = true;
            this.macAddressesLocked.Location = new System.Drawing.Point(6, 210);
            this.macAddressesLocked.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.macAddressesLocked.Name = "macAddressesLocked";
            this.macAddressesLocked.Size = new System.Drawing.Size(205, 29);
            this.macAddressesLocked.TabIndex = 27;
            this.macAddressesLocked.Text = "MAC Addresses:";
            this.macAddressesLocked.UseVisualStyleBackColor = true;
            // 
            // requestedDateTimeLabel
            // 
            this.requestedDateTimeLabel.AutoSize = true;
            this.requestedDateTimeLabel.Location = new System.Drawing.Point(38, 12);
            this.requestedDateTimeLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.requestedDateTimeLabel.Name = "requestedDateTimeLabel";
            this.requestedDateTimeLabel.Size = new System.Drawing.Size(173, 25);
            this.requestedDateTimeLabel.TabIndex = 28;
            this.requestedDateTimeLabel.Text = "Requested Date:";
            // 
            // licenseRequestID
            // 
            this.licenseRequestID.BackColor = System.Drawing.SystemColors.Control;
            this.licenseRequestID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.licenseRequestID.Location = new System.Drawing.Point(892, 12);
            this.licenseRequestID.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.licenseRequestID.Name = "licenseRequestID";
            this.licenseRequestID.ReadOnly = true;
            this.licenseRequestID.Size = new System.Drawing.Size(570, 24);
            this.licenseRequestID.TabIndex = 29;
            // 
            // licenseRequestIDLabel
            // 
            this.licenseRequestIDLabel.AutoSize = true;
            this.licenseRequestIDLabel.Location = new System.Drawing.Point(752, 12);
            this.licenseRequestIDLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.licenseRequestIDLabel.Name = "licenseRequestIDLabel";
            this.licenseRequestIDLabel.Size = new System.Drawing.Size(124, 25);
            this.licenseRequestIDLabel.TabIndex = 31;
            this.licenseRequestIDLabel.Text = "Request ID:";
            // 
            // expires
            // 
            this.expires.AutoSize = true;
            this.expires.Location = new System.Drawing.Point(6, 260);
            this.expires.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.expires.Name = "expires";
            this.expires.Size = new System.Drawing.Size(122, 29);
            this.expires.TabIndex = 32;
            this.expires.Text = "Expires:";
            this.expires.UseVisualStyleBackColor = true;
            // 
            // expiresDateTime
            // 
            this.expiresDateTime.Location = new System.Drawing.Point(144, 256);
            this.expiresDateTime.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.expiresDateTime.Name = "expiresDateTime";
            this.expiresDateTime.Size = new System.Drawing.Size(526, 31);
            this.expiresDateTime.TabIndex = 33;
            this.expiresDateTime.ValueChanged += new System.EventHandler(this.expiresDateTime_ValueChanged);
            // 
            // majorVersionLocked
            // 
            this.majorVersionLocked.AutoSize = true;
            this.majorVersionLocked.Location = new System.Drawing.Point(6, 310);
            this.majorVersionLocked.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.majorVersionLocked.Name = "majorVersionLocked";
            this.majorVersionLocked.Size = new System.Drawing.Size(123, 29);
            this.majorVersionLocked.TabIndex = 34;
            this.majorVersionLocked.Text = "Version:";
            this.majorVersionLocked.UseVisualStyleBackColor = true;
            // 
            // majorVersion
            // 
            this.majorVersion.BackColor = System.Drawing.SystemColors.Control;
            this.majorVersion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.majorVersion.Location = new System.Drawing.Point(146, 312);
            this.majorVersion.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.majorVersion.Name = "majorVersion";
            this.majorVersion.ReadOnly = true;
            this.majorVersion.Size = new System.Drawing.Size(66, 24);
            this.majorVersion.TabIndex = 35;
            // 
            // licenseRequestPanel
            // 
            this.licenseRequestPanel.Controls.Add(this.leaseDurationUnitsLabel);
            this.licenseRequestPanel.Controls.Add(this.leaseDuration);
            this.licenseRequestPanel.Controls.Add(this.leaseDurationLabel);
            this.licenseRequestPanel.Controls.Add(this.leasesLabel);
            this.licenseRequestPanel.Controls.Add(this.leases);
            this.licenseRequestPanel.Controls.Add(this.serverCertificate);
            this.licenseRequestPanel.Controls.Add(this.expiresDetails);
            this.licenseRequestPanel.Controls.Add(this.licenseRequestedDateTime);
            this.licenseRequestPanel.Controls.Add(this.build);
            this.licenseRequestPanel.Controls.Add(this.buildLocked);
            this.licenseRequestPanel.Controls.Add(this.revisionLocked);
            this.licenseRequestPanel.Controls.Add(this.revision);
            this.licenseRequestPanel.Controls.Add(this.minorVersionLocked);
            this.licenseRequestPanel.Controls.Add(this.minorVersion);
            this.licenseRequestPanel.Controls.Add(this.majorVersionLocked);
            this.licenseRequestPanel.Controls.Add(this.majorVersion);
            this.licenseRequestPanel.Controls.Add(this.requestedDateTimeLabel);
            this.licenseRequestPanel.Controls.Add(this.licenseRequestIDLabel);
            this.licenseRequestPanel.Controls.Add(this.expires);
            this.licenseRequestPanel.Controls.Add(this.expiresDateTime);
            this.licenseRequestPanel.Controls.Add(this.licenseRequestID);
            this.licenseRequestPanel.Controls.Add(this.userName);
            this.licenseRequestPanel.Controls.Add(this.macAddressesLocked);
            this.licenseRequestPanel.Controls.Add(this.userNameLocked);
            this.licenseRequestPanel.Controls.Add(this.macAddresses);
            this.licenseRequestPanel.Controls.Add(this.userSIDLocked);
            this.licenseRequestPanel.Controls.Add(this.cpuID);
            this.licenseRequestPanel.Controls.Add(this.userSID);
            this.licenseRequestPanel.Controls.Add(this.cpuIDLocked);
            this.licenseRequestPanel.Controls.Add(this.biosSerialNumberLocked);
            this.licenseRequestPanel.Controls.Add(this.biosSerialNumber);
            this.licenseRequestPanel.Location = new System.Drawing.Point(82, 617);
            this.licenseRequestPanel.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.licenseRequestPanel.Name = "licenseRequestPanel";
            this.licenseRequestPanel.Size = new System.Drawing.Size(1468, 402);
            this.licenseRequestPanel.TabIndex = 36;
            // 
            // leaseDurationUnitsLabel
            // 
            this.leaseDurationUnitsLabel.AutoSize = true;
            this.leaseDurationUnitsLabel.Location = new System.Drawing.Point(916, 356);
            this.leaseDurationUnitsLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.leaseDurationUnitsLabel.Name = "leaseDurationUnitsLabel";
            this.leaseDurationUnitsLabel.Size = new System.Drawing.Size(87, 25);
            this.leaseDurationUnitsLabel.TabIndex = 50;
            this.leaseDurationUnitsLabel.Text = "minutes";
            // 
            // leaseDuration
            // 
            this.leaseDuration.Location = new System.Drawing.Point(704, 350);
            this.leaseDuration.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.leaseDuration.Name = "leaseDuration";
            this.leaseDuration.Size = new System.Drawing.Size(196, 31);
            this.leaseDuration.TabIndex = 49;
            // 
            // leaseDurationLabel
            // 
            this.leaseDurationLabel.AutoSize = true;
            this.leaseDurationLabel.Location = new System.Drawing.Point(528, 356);
            this.leaseDurationLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.leaseDurationLabel.Name = "leaseDurationLabel";
            this.leaseDurationLabel.Size = new System.Drawing.Size(164, 25);
            this.leaseDurationLabel.TabIndex = 48;
            this.leaseDurationLabel.Text = "Lease Duration:";
            // 
            // leasesLabel
            // 
            this.leasesLabel.AutoSize = true;
            this.leasesLabel.Location = new System.Drawing.Point(216, 356);
            this.leasesLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.leasesLabel.Name = "leasesLabel";
            this.leasesLabel.Size = new System.Drawing.Size(88, 25);
            this.leasesLabel.TabIndex = 47;
            this.leasesLabel.Text = "Leases:";
            // 
            // leases
            // 
            this.leases.Location = new System.Drawing.Point(316, 350);
            this.leases.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.leases.Name = "leases";
            this.leases.Size = new System.Drawing.Size(196, 31);
            this.leases.TabIndex = 46;
            // 
            // serverCertificate
            // 
            this.serverCertificate.AutoSize = true;
            this.serverCertificate.Location = new System.Drawing.Point(4, 354);
            this.serverCertificate.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.serverCertificate.Name = "serverCertificate";
            this.serverCertificate.Size = new System.Drawing.Size(194, 29);
            this.serverCertificate.TabIndex = 45;
            this.serverCertificate.Text = "Server License:";
            this.serverCertificate.UseVisualStyleBackColor = true;
            // 
            // expiresDetails
            // 
            this.expiresDetails.BackColor = System.Drawing.SystemColors.Control;
            this.expiresDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.expiresDetails.Location = new System.Drawing.Point(686, 262);
            this.expiresDetails.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.expiresDetails.Name = "expiresDetails";
            this.expiresDetails.ReadOnly = true;
            this.expiresDetails.Size = new System.Drawing.Size(776, 24);
            this.expiresDetails.TabIndex = 44;
            // 
            // licenseRequestedDateTime
            // 
            this.licenseRequestedDateTime.BackColor = System.Drawing.SystemColors.Control;
            this.licenseRequestedDateTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.licenseRequestedDateTime.Location = new System.Drawing.Point(226, 12);
            this.licenseRequestedDateTime.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.licenseRequestedDateTime.Name = "licenseRequestedDateTime";
            this.licenseRequestedDateTime.ReadOnly = true;
            this.licenseRequestedDateTime.Size = new System.Drawing.Size(496, 24);
            this.licenseRequestedDateTime.TabIndex = 43;
            // 
            // build
            // 
            this.build.BackColor = System.Drawing.SystemColors.Control;
            this.build.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.build.Location = new System.Drawing.Point(506, 312);
            this.build.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.build.Name = "build";
            this.build.ReadOnly = true;
            this.build.Size = new System.Drawing.Size(66, 24);
            this.build.TabIndex = 41;
            // 
            // buildLocked
            // 
            this.buildLocked.AutoSize = true;
            this.buildLocked.Location = new System.Drawing.Point(464, 310);
            this.buildLocked.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.buildLocked.Name = "buildLocked";
            this.buildLocked.Size = new System.Drawing.Size(28, 27);
            this.buildLocked.TabIndex = 40;
            this.buildLocked.UseVisualStyleBackColor = true;
            // 
            // revisionLocked
            // 
            this.revisionLocked.AutoSize = true;
            this.revisionLocked.Location = new System.Drawing.Point(344, 310);
            this.revisionLocked.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.revisionLocked.Name = "revisionLocked";
            this.revisionLocked.Size = new System.Drawing.Size(28, 27);
            this.revisionLocked.TabIndex = 39;
            this.revisionLocked.UseVisualStyleBackColor = true;
            // 
            // revision
            // 
            this.revision.BackColor = System.Drawing.SystemColors.Control;
            this.revision.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.revision.Location = new System.Drawing.Point(386, 312);
            this.revision.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.revision.Name = "revision";
            this.revision.ReadOnly = true;
            this.revision.Size = new System.Drawing.Size(66, 24);
            this.revision.TabIndex = 38;
            // 
            // minorVersionLocked
            // 
            this.minorVersionLocked.AutoSize = true;
            this.minorVersionLocked.Location = new System.Drawing.Point(224, 310);
            this.minorVersionLocked.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.minorVersionLocked.Name = "minorVersionLocked";
            this.minorVersionLocked.Size = new System.Drawing.Size(28, 27);
            this.minorVersionLocked.TabIndex = 37;
            this.minorVersionLocked.UseVisualStyleBackColor = true;
            // 
            // minorVersion
            // 
            this.minorVersion.BackColor = System.Drawing.SystemColors.Control;
            this.minorVersion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.minorVersion.Location = new System.Drawing.Point(266, 312);
            this.minorVersion.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.minorVersion.Name = "minorVersion";
            this.minorVersion.ReadOnly = true;
            this.minorVersion.Size = new System.Drawing.Size(66, 24);
            this.minorVersion.TabIndex = 36;
            // 
            // CreateLicenseCertificateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1568, 1131);
            this.Controls.Add(this.licenseRequestPanel);
            this.Controls.Add(this.rememberSystemPassword);
            this.Controls.Add(this.showSystemPassword);
            this.Controls.Add(this.step3);
            this.Controls.Add(this.step2);
            this.Controls.Add(this.step1);
            this.Controls.Add(this.ok);
            this.Controls.Add(this.step4a);
            this.Controls.Add(this.step4);
            this.Controls.Add(this.clipboardContents);
            this.Controls.Add(this.systemPassword);
            this.Controls.Add(this.step3a);
            this.Controls.Add(this.step2a);
            this.Controls.Add(this.step1a);
            this.Controls.Add(this.step0);
            this.Controls.Add(this.copyLicenseCertificateToClipboard);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "CreateLicenseCertificateForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "ZydecoSoftwareLLC, LLC :: Create License";
            this.licenseRequestPanel.ResumeLayout(false);
            this.licenseRequestPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label step0;
        private System.Windows.Forms.Label step1a;
        private System.Windows.Forms.TextBox clipboardContents;
        private System.Windows.Forms.Label step2a;
        private System.Windows.Forms.Label step3a;
        private System.Windows.Forms.TextBox systemPassword;
        private System.Windows.Forms.Label step4;
        private System.Windows.Forms.Label step4a;
        private System.Windows.Forms.LinkLabel copyLicenseCertificateToClipboard;
        private System.Windows.Forms.Button ok;
        private System.Windows.Forms.Label step1;
        private System.Windows.Forms.Label step2;
        private System.Windows.Forms.Label step3;
        private System.Windows.Forms.CheckBox showSystemPassword;
        private System.Windows.Forms.CheckBox rememberSystemPassword;
        private System.Windows.Forms.CheckBox userNameLocked;
        private System.Windows.Forms.TextBox userName;
        private System.Windows.Forms.CheckBox userSIDLocked;
        private System.Windows.Forms.TextBox userSID;
        private System.Windows.Forms.CheckBox biosSerialNumberLocked;
        private System.Windows.Forms.TextBox biosSerialNumber;
        private System.Windows.Forms.CheckBox cpuIDLocked;
        private System.Windows.Forms.TextBox cpuID;
        private System.Windows.Forms.TextBox macAddresses;
        private System.Windows.Forms.CheckBox macAddressesLocked;
        private System.Windows.Forms.Label requestedDateTimeLabel;
        private System.Windows.Forms.TextBox licenseRequestID;
        private System.Windows.Forms.Label licenseRequestIDLabel;
        private System.Windows.Forms.CheckBox expires;
        private System.Windows.Forms.DateTimePicker expiresDateTime;
        private System.Windows.Forms.CheckBox majorVersionLocked;
        private System.Windows.Forms.TextBox majorVersion;
        private System.Windows.Forms.Panel licenseRequestPanel;
        private System.Windows.Forms.TextBox build;
        private System.Windows.Forms.CheckBox buildLocked;
        private System.Windows.Forms.CheckBox revisionLocked;
        private System.Windows.Forms.TextBox revision;
        private System.Windows.Forms.CheckBox minorVersionLocked;
        private System.Windows.Forms.TextBox minorVersion;
        private System.Windows.Forms.TextBox licenseRequestedDateTime;
        private System.Windows.Forms.TextBox expiresDetails;
        private System.Windows.Forms.Label leaseDurationUnitsLabel;
        private System.Windows.Forms.TextBox leaseDuration;
        private System.Windows.Forms.Label leaseDurationLabel;
        private System.Windows.Forms.Label leasesLabel;
        private System.Windows.Forms.TextBox leases;
        private System.Windows.Forms.CheckBox serverCertificate;
    }
}
