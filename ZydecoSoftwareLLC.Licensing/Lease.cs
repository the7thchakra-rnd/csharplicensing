﻿/**
 * Lease.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace ZydecoSoftwareLLC.Licensing {
    [Serializable]
    public sealed class Lease {

        private readonly LicenseRequest request;

        private readonly LicenseCertificate certificate;

        private DateTime lastPolled = DateTime.UtcNow;

        public LicenseRequest LicenseRequest {
            get { return request; }
        }

        public LicenseCertificate LicenseCertificate {
            get { return certificate; }
        }

        internal Lease(LicenseRequest request, LicenseCertificate certificate) {
            this.request = request;
            this.certificate = certificate;
        }

        internal Lease(object obj) {
            try {
                object request = obj.GetType().GetField("request",
                    BindingFlags.NonPublic | BindingFlags.Instance).GetValue(obj);
                this.request = new LicenseRequest(request);
            }
            catch {
            }

            try {
                object certificate = obj.GetType().GetField("certificate",
                    BindingFlags.NonPublic | BindingFlags.Instance).GetValue(obj);
                this.certificate = new LicenseCertificate(certificate);
            }
            catch {
            }
        }

        internal bool IsExpired(DateTime now) {
            return (now >= certificate.ExpiresDateTime);
        }

        public string UserName {
            get { return request.UserName; }
        }

        public DateTime ExpiresDateTime {
            get { return certificate.ExpiresDateTime; }
        }

        public DateTime LastPolled {
            get { return lastPolled; }
        }

        internal void Touch() {
            lastPolled = DateTime.UtcNow;
        }

        internal bool IsAboutToExpire(int minutes) {
            return (DateTime.UtcNow.AddMinutes(minutes) >= ExpiresDateTime);
        }
    }
}
