﻿namespace ZydecoSoftwareLLC.Licensing {
    partial class CopyrightSplashForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CopyrightSplashForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.reason = new System.Windows.Forms.TextBox();
            this.requestLicense = new System.Windows.Forms.Button();
            this.ok = new System.Windows.Forms.Button();
            this.header0 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tcpEndPointLabel = new System.Windows.Forms.Label();
            this.tcpEndPoint = new System.Windows.Forms.TextBox();
            this.tcpEndPointIndicator = new System.Windows.Forms.Label();
            this.statusLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(24, 23);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 237);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // reason
            // 
            this.reason.BackColor = System.Drawing.SystemColors.Control;
            this.reason.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reason.Location = new System.Drawing.Point(236, 94);
            this.reason.Margin = new System.Windows.Forms.Padding(6);
            this.reason.Name = "reason";
            this.reason.ReadOnly = true;
            this.reason.Size = new System.Drawing.Size(1104, 24);
            this.reason.TabIndex = 23;
            this.reason.TabStop = false;
            // 
            // requestLicense
            // 
            this.requestLicense.Location = new System.Drawing.Point(884, 215);
            this.requestLicense.Margin = new System.Windows.Forms.Padding(6);
            this.requestLicense.Name = "requestLicense";
            this.requestLicense.Size = new System.Drawing.Size(300, 44);
            this.requestLicense.TabIndex = 24;
            this.requestLicense.Text = "&Request License";
            this.requestLicense.UseVisualStyleBackColor = true;
            this.requestLicense.Click += new System.EventHandler(this.requestLicense_Click);
            // 
            // ok
            // 
            this.ok.Location = new System.Drawing.Point(1196, 215);
            this.ok.Margin = new System.Windows.Forms.Padding(6);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(150, 44);
            this.ok.TabIndex = 25;
            this.ok.Text = "&OK";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // header0
            // 
            this.header0.AutoSize = true;
            this.header0.Location = new System.Drawing.Point(236, 23);
            this.header0.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.header0.Name = "header0";
            this.header0.Size = new System.Drawing.Size(373, 25);
            this.header0.TabIndex = 26;
            this.header0.Text = "ZydecoSoftwareLLC, LLC :: Licensing";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(236, 56);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(479, 25);
            this.label1.TabIndex = 27;
            this.label1.Text = "Copyright ©2011-2023 ZydecoSoftwareLLC, LLC";
            // 
            // tcpEndPointLabel
            // 
            this.tcpEndPointLabel.AutoSize = true;
            this.tcpEndPointLabel.Location = new System.Drawing.Point(236, 177);
            this.tcpEndPointLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.tcpEndPointLabel.Name = "tcpEndPointLabel";
            this.tcpEndPointLabel.Size = new System.Drawing.Size(162, 25);
            this.tcpEndPointLabel.TabIndex = 29;
            this.tcpEndPointLabel.Text = "License Server:";
            // 
            // tcpEndPoint
            // 
            this.tcpEndPoint.Enabled = false;
            this.tcpEndPoint.Location = new System.Drawing.Point(410, 171);
            this.tcpEndPoint.Margin = new System.Windows.Forms.Padding(6);
            this.tcpEndPoint.Name = "tcpEndPoint";
            this.tcpEndPoint.Size = new System.Drawing.Size(458, 31);
            this.tcpEndPoint.TabIndex = 30;
            this.tcpEndPoint.TextChanged += new System.EventHandler(this.tcpEndPoint_TextChanged);
            // 
            // tcpEndPointIndicator
            // 
            this.tcpEndPointIndicator.Location = new System.Drawing.Point(878, 177);
            this.tcpEndPointIndicator.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.tcpEndPointIndicator.Name = "tcpEndPointIndicator";
            this.tcpEndPointIndicator.Size = new System.Drawing.Size(462, 33);
            this.tcpEndPointIndicator.TabIndex = 31;
            // 
            // statusLabel
            // 
            this.statusLabel.Location = new System.Drawing.Point(236, 215);
            this.statusLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(630, 44);
            this.statusLabel.TabIndex = 32;
            this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CopyrightSplashForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 273);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.tcpEndPointIndicator);
            this.Controls.Add(this.tcpEndPoint);
            this.Controls.Add(this.tcpEndPointLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.header0);
            this.Controls.Add(this.ok);
            this.Controls.Add(this.requestLicense);
            this.Controls.Add(this.reason);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CopyrightSplashForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "ZydecoSoftwareLLC, LLC :: Licensing";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox reason;
        private System.Windows.Forms.Button requestLicense;
        private System.Windows.Forms.Button ok;
        private System.Windows.Forms.Label header0;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label tcpEndPointLabel;
        private System.Windows.Forms.TextBox tcpEndPoint;
        private System.Windows.Forms.Label tcpEndPointIndicator;
        private System.Windows.Forms.Label statusLabel;
    }
}
