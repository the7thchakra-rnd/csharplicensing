﻿/**
 * LicensingServices.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ZydecoSoftwareLLC.Licensing {
    [ComVisible(true)]
    public class LicensingServices {
        private static DateTime splashLastShown = new DateTime(0);

        public static void EnsureIsLicensed() {
            EnsureIsLicensed(false);
        }

        public static void EnsureIsLicensed(bool server) {
            EnsureIsLicensed(server, true);
        }

        public static void EnsureIsLicensed(bool server, bool showInfoDialog) {
            try {
                StackFrame stackFrame = new StackFrame(1);
                EnsureIsLicensed(server, showInfoDialog, stackFrame);
            }
            catch (LicenseException cause) {
                throw cause;
            }
            catch (Exception cause) {
                LicenseException exception = new LicenseException("An unexpected exception occurred.", cause);
                throw exception;
            }
        }

        public static void EnsureIsLicensed(StackFrame stackFrame) {
            EnsureIsLicensed(false, true, stackFrame);
        }

        public static void EnsureIsLicensed(bool server, bool showInfoDialog, StackFrame stackFrame) {
            if (showInfoDialog) {
                ShowSplash(server);
            }
            
            try {
                if (stackFrame == null) {
                    ArgumentNullException exception = new ArgumentNullException("stackFrame");
                    throw exception;
                }

                MethodBase method = stackFrame.GetMethod();

                if ((License.lease != null) && !License.lease.IsAboutToExpire(0)) {
                    return;
                }

                License license = License.Instance;
                license.EnsureIsLicensed(server, showInfoDialog, method);
            }
            catch (LicenseException cause) {
                throw cause;
            }
            catch (Exception cause) {
                LicenseException exception = new LicenseException("An unexpected exception occurred.", cause);
                throw exception;
            }
        }

        private static void ShowSplash(bool server) {
            bool showSplash;
            string reason;
            DateTime now = DateTime.UtcNow;
            bool valid;
            if (splashLastShown.Ticks == 0) {
                showSplash = true;
                reason = License.Instance.GetBriefInfo(server, out valid);
            }
            else {
                reason = License.Instance.GetBriefInfo(server, out valid);
                if (reason == null) {
                    showSplash = false;
                }
                else if ((now - splashLastShown).TotalDays >= 1) {
                    showSplash = true;
                }
                else {
                    showSplash = false;
                }
            }

            if ((License.lease != null) && License.lease.IsAboutToExpire(1)) {
                showSplash = true;
                reason = "Your lease is about to expire.";
            }

            if (showSplash && !string.IsNullOrEmpty(reason)) {
                splashLastShown = new DateTime((int)(now.Ticks / TimeSpan.TicksPerDay) * TimeSpan.TicksPerDay);

                CopyrightSplashForm copyrightSplashForm = new CopyrightSplashForm(server);
                copyrightSplashForm.Reason = reason;
                copyrightSplashForm.IsLicenseValid = valid;
                copyrightSplashForm.ShowDialog();
            }
        }

        public static byte[] ToByteArray(object obj) {
            byte[] data;
            using (MemoryStream buffer = new MemoryStream()) {
                Serialize(obj, buffer);
                data = buffer.ToArray();
            }

            return data;
        }

        public static void Serialize(object obj, Stream stream) {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, obj);
        }

        public static void Serialize(object obj, string path) {
            byte[] bytes;
            using (MemoryStream buffer = new MemoryStream()) {
                Serialize(obj, buffer);

                bytes = buffer.ToArray();
            }
            File.WriteAllBytes(path, bytes);
        }

        public static object ToObject(byte[] b, int offset, int length) {
            object obj;
            using (MemoryStream buffer = new MemoryStream(b, offset, length)) {
                obj = ToObject(buffer);
            }

            return obj;
        }

        public static object ToObject(Stream stream) {
            object obj;
            BinaryFormatter formatter = new BinaryFormatter();
            obj = formatter.Deserialize(stream);

            return obj;
        }

        public static object ToObject(string path) {
            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                return ToObject(stream);
            }
        }

    }
}
