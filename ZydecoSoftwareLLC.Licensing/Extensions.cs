﻿/**
 * Extensions.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ZydecoSoftwareLLC.Licensing {

    /// <summary>
    /// Extensions to <see cref="BinaryReader"/>.
    /// </summary>
    internal static class Extensions {

        /// <summary>
        /// Read a length-prefixed block of bytes.
        /// </summary>
        /// <param name="self">the <see cref="BinaryReader"/></param>
        /// <returns>the bytes</returns>
        internal static string Join(this IEnumerable<string> self, string separator, string prefix = null, string suffix = null) {
            string text;
            StringBuilder builder = new StringBuilder();
            
            if (prefix != null) {
                builder.Append(prefix);
            }

            if (self != null) {
                bool first = true;
                foreach (string item in self) {
                    if (first) {
                        first = false;
                    }
                    else {
                        builder.Append(separator);
                    }

                    builder.Append(item);
                }
            }

            if (suffix != null) {
                builder.Append(suffix);
            }

            text = builder.ToString();

            return text;
        }
    }
}
