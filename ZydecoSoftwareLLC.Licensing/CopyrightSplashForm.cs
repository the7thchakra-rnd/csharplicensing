﻿/**
 * CopyrightSplashForm.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace ZydecoSoftwareLLC.Licensing {
    public partial class CopyrightSplashForm: Form {
        private int elapsed;

        private string reasonText;

        private readonly int licenseCheckLimit = 0;

        private bool closed;

        public string Reason {
            get { return reasonText; }
            set {
                reasonText = value;
                if (reason != null) {
                    reason.Text = reasonText;
                }
                else {
                    reason.Text = string.Empty;
                }
            }
        }

        public bool IsLicenseValid {
            get { return ok.Enabled; }
            set { ok.Enabled = value; }
        }

        private readonly bool server;

        private string GetElapsedText() {
            if (licenseCheckLimit > 0) {
                return (" (" + Math.Max(licenseCheckLimit - elapsed, 0) + " sec. remaining)");
            }
            else {
                return (" (" + elapsed + " sec. elapsed)");
            }
        }

        public CopyrightSplashForm(bool server) {
            InitializeComponent();

            this.server = server;

            CenterToScreen();

            statusLabel.Text = ("Waiting for license server" + GetElapsedText());

            if (!server) {
                ThreadPool.QueueUserWorkItem(WaitForLicenseServer);
            }
            else {
                tcpEndPointLabel.Visible = false;
                tcpEndPoint.Visible = false;
                tcpEndPointIndicator.Visible = false;
                statusLabel.Visible = false;
                ok.Enabled = true;
                requestLicense.Enabled = true;
            }

            string initialTCPEndPoint;
            License.GetLicenseServerTCPEndPoint(out initialTCPEndPoint);
            tcpEndPoint.Text = initialTCPEndPoint;
        }

        private void WaitForLicenseServer(object arg) {
            while (UpdateProgressBar()) {
                Thread.Sleep(1000);
            }

            UpdateInfo();
        }

        private delegate void UpdateInfoDelegate();

        private void UpdateInfo() {
            if (InvokeRequired) {
                Invoke(new UpdateInfoDelegate(UpdateInfo));
            }
            else {
                requestLicense.Enabled = true;
                ok.Enabled = true;

                bool valid;
                Reason = License.Instance.GetBriefInfo(server, out valid);
            }
        }

        private delegate bool UpdateProgressBarDelegate();

        private bool UpdateProgressBar() {
            if (InvokeRequired) {
                return (bool)Invoke(new UpdateProgressBarDelegate(UpdateProgressBar));
            }
            else {
                if ((licenseCheckLimit > 0) && (elapsed >= licenseCheckLimit)) {
                    if (License.IsLicenseServerAvailable) {
                        Reason = "A lease could not be acquired.  Please wait or contact your administrator.";
                    }

                    return false;
                }

                if (elapsed >= 5) {
                    requestLicense.Enabled = true;
                    ok.Enabled = true;
                    tcpEndPoint.Enabled = true;
                }
                else {
                    requestLicense.Enabled = false;
                    ok.Enabled = false;
                    tcpEndPoint.Enabled = false;
                }

                elapsed += 1;

                if (License.licenseServerAvailableMessage != null) {
                    tcpEndPointIndicator.Text = (License.licenseServerAvailableMessage.Address + ":" + License.licenseServerAvailableMessage.Port);
                }
                else {
                    tcpEndPointIndicator.Text = "";
                }

                if (License.IsLicenseServerAvailable) {
                    if ((License.IsLeaseAvailable != true) && !License.noLeasesAvailable && ((License.lease == null) || !License.lease.IsAboutToExpire(0))) {
                        statusLabel.Text = ("Found license server.  Waiting for lease" + GetElapsedText());
                    }
                    else if (License.noLeasesAvailable) {
                        statusLabel.Text = ("A lease could not be acquired.  Waiting...");
                    }
                    else {
                        statusLabel.Text = ("Lease acquired");

                        ThreadPool.QueueUserWorkItem(delegate(object closure) {
                            Thread.Sleep(500);
                            CloseInternal();
                        });

                        return false;
                    }
                }
                else if ((License.IsLeaseAvailable == true) && (License.lease != null) && !License.lease.IsAboutToExpire(0)) {
                    statusLabel.Text = ("Lease acquired");

                    ThreadPool.QueueUserWorkItem(delegate(object closure) {
                        Thread.Sleep(2000);
                        CloseInternal();
                    });

                    return false;
                }
                else {
                    statusLabel.Text = ("Waiting for license server" + GetElapsedText());
                }

                return true;
            }
        }

        private void requestLicense_Click(object sender, EventArgs e) {
            RequestLicenseForm requestLicenseForm = new RequestLicenseForm();
            requestLicenseForm.Reason = reasonText;
            requestLicenseForm.ShowDialog();

            Close();
        }

        private void ok_Click(object sender, EventArgs e) {
            Close();
        }

        private void tcpEndPoint_TextChanged(object sender, EventArgs eventArgs) {
            string text = tcpEndPoint.Text;

            ThreadPool.QueueUserWorkItem(delegate(object argument)
            {
                License.SetLicenseServerTCPEndPoint(text);

                if (!string.IsNullOrEmpty(text)) {
                    if (!closed) {
                        Thread.Sleep(5000);

                        tcpEndPoint_TextChanged(null, null);
                    }
                }
            });
        }

        protected override void OnClosed(EventArgs eventArgs) {
            base.OnClosed(eventArgs);

            closed = true;
        }

        private delegate void CloseInternalDelegate();

        private void CloseInternal() {
            if (InvokeRequired) {
                Invoke(new CloseInternalDelegate(CloseInternal));
            }
            else {
                Close();
            }
        }
    }
}
