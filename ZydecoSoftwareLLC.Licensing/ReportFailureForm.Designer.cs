﻿namespace ZydecoSoftwareLLC.Licensing {
    partial class ReportFailureForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportFailureForm));
            this.header = new System.Windows.Forms.Label();
            this.exceptionDetails = new System.Windows.Forms.TextBox();
            this.ok = new System.Windows.Forms.Button();
            this.sendEmail = new System.Windows.Forms.LinkLabel();
            this.request0a = new System.Windows.Forms.Label();
            this.request0b = new System.Windows.Forms.Label();
            this.mailtoAddress = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // header
            // 
            this.header.AutoSize = true;
            this.header.Location = new System.Drawing.Point(26, 25);
            this.header.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(344, 25);
            this.header.TabIndex = 0;
            this.header.Text = "An unexpected error has occurred:";
            // 
            // exceptionDetails
            // 
            this.exceptionDetails.Location = new System.Drawing.Point(26, 69);
            this.exceptionDetails.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.exceptionDetails.Multiline = true;
            this.exceptionDetails.Name = "exceptionDetails";
            this.exceptionDetails.ReadOnly = true;
            this.exceptionDetails.Size = new System.Drawing.Size(1514, 881);
            this.exceptionDetails.TabIndex = 1;
            // 
            // ok
            // 
            this.ok.Location = new System.Drawing.Point(1394, 1013);
            this.ok.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(150, 44);
            this.ok.TabIndex = 5;
            this.ok.Text = "&OK";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // sendEmail
            // 
            this.sendEmail.AutoSize = true;
            this.sendEmail.Location = new System.Drawing.Point(670, 971);
            this.sendEmail.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.sendEmail.Name = "sendEmail";
            this.sendEmail.Size = new System.Drawing.Size(153, 25);
            this.sendEmail.TabIndex = 26;
            this.sendEmail.TabStop = true;
            this.sendEmail.Text = "send an e-mail";
            this.sendEmail.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.sendEmail_LinkClicked);
            // 
            // request0a
            // 
            this.request0a.AutoSize = true;
            this.request0a.Location = new System.Drawing.Point(20, 971);
            this.request0a.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.request0a.Name = "request0a";
            this.request0a.Size = new System.Drawing.Size(632, 25);
            this.request0a.TabIndex = 27;
            this.request0a.Text = "The exception details have been copied to the clipboard.  Please";
            // 
            // request0b
            // 
            this.request0b.AutoSize = true;
            this.request0b.Location = new System.Drawing.Point(822, 971);
            this.request0b.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.request0b.Name = "request0b";
            this.request0b.Size = new System.Drawing.Size(30, 25);
            this.request0b.TabIndex = 28;
            this.request0b.Text = "to";
            // 
            // mailtoAddress
            // 
            this.mailtoAddress.BackColor = System.Drawing.SystemColors.Control;
            this.mailtoAddress.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.mailtoAddress.Location = new System.Drawing.Point(866, 971);
            this.mailtoAddress.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.mailtoAddress.Name = "mailtoAddress";
            this.mailtoAddress.ReadOnly = true;
            this.mailtoAddress.Size = new System.Drawing.Size(678, 24);
            this.mailtoAddress.TabIndex = 29;
            // 
            // ReportFailureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1568, 1081);
            this.Controls.Add(this.mailtoAddress);
            this.Controls.Add(this.request0b);
            this.Controls.Add(this.request0a);
            this.Controls.Add(this.sendEmail);
            this.Controls.Add(this.ok);
            this.Controls.Add(this.exceptionDetails);
            this.Controls.Add(this.header);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "ReportFailureForm";
            this.Text = "ZydecoSoftwareLLC, LLC :: Failure";
            this.Load += new System.EventHandler(this.ReportFailureForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label header;
        private System.Windows.Forms.TextBox exceptionDetails;
        private System.Windows.Forms.Button ok;
        private System.Windows.Forms.LinkLabel sendEmail;
        private System.Windows.Forms.Label request0a;
        private System.Windows.Forms.Label request0b;
        private System.Windows.Forms.TextBox mailtoAddress;
    }
}
