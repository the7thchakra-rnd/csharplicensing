﻿/**
 * PublicKeyEncrypt.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using System;
using System.IO;
using System.Security.Cryptography;

namespace ZydecoSoftwareLLC.Licensing {
    internal class PublicKeyEncrypt {
        internal static byte[] Encrypt(byte[] bytes) {
            byte[] encrypted;

            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

            // Initialize the algorithm from the public key parameters:

            byte[] csp = new byte[] {
                0x06, 0x02, 0x00, 0x00, 0x00, 0xa4, 0x00, 0x00, 0x52, 0x53, 0x41, 0x31, 0x00, 0x04, 0x00,
                0x00, 0x01, 0x00, 0x01, 0x00, 0x7b, 0x7b, 0xdf, 0xc7, 0xfb, 0x34, 0x94, 0x26, 0x46, 0xac, 0x6c,
                0x3f, 0x27, 0xb5, 0xec, 0xd0, 0x2b, 0x7e, 0x4f, 0x54, 0x10, 0x59, 0xcf, 0xcc, 0x00, 0x5a, 0x1d,
                0x21, 0xdb, 0x98, 0xc9, 0x2a, 0x12, 0xaf, 0x78, 0x76, 0x8c, 0x28, 0xcc, 0xd8, 0x7c, 0xf3, 0xa1,
                0x73, 0x71, 0x3d, 0xf6, 0x13, 0xba, 0x03, 0xe8, 0x9f, 0xf0, 0xe0, 0xae, 0x06, 0x75, 0x04, 0x2f,
                0xdb, 0x02, 0x16, 0x05, 0x5b, 0x51, 0xda, 0x34, 0x92, 0xf5, 0xa0, 0xc5, 0x88, 0xe5, 0x97, 0x73,
                0xf2, 0x21, 0xe6, 0x1f, 0x94, 0xb4, 0x47, 0xb4, 0xf8, 0xa3, 0x8d, 0x2f, 0x6c, 0x50, 0x87, 0x5c,
                0x29, 0xb7, 0xa7, 0xbf, 0xbe, 0xc1, 0x1f, 0x23, 0x66, 0xe4, 0x76, 0x19, 0xd7, 0x89, 0x85, 0xe6,
                0x2b, 0xe5, 0x5c, 0x3b, 0x36, 0xfd, 0x49, 0x22, 0x08, 0x6f, 0x26, 0x57, 0x49, 0x31, 0xf0, 0x53,
                0x73, 0x48, 0x49, 0xbe, 0xaa
            };

            rsa.ImportCspBlob(csp);

            using (MemoryStream buffer = new MemoryStream()) {

                // Create Rinjadel encryption parameters and store them in the stream.
                // The initialization vector (IV) and key are prefixed
                // by their length and then encyphered using the public key:
                //
                //   +-----------+-...-+------------+-...-+
                //   + iv.Length | iv  | key.Length | key |
                //   +-----------+-...-+------------+-...-+

                RijndaelManaged rijndael = new RijndaelManaged();
                rijndael.GenerateIV();
                rijndael.GenerateKey();

                byte[] iv = rijndael.IV;
                byte[] key = rijndael.Key;
                int niv = iv.Length;
                int nkey = key.Length;
                byte[] parameterBlock = new byte[2 + niv + nkey];
                parameterBlock[0] = (byte)niv;
                parameterBlock[1] = (byte)nkey;
                Array.Copy(iv, 0, parameterBlock, 2, niv);
                Array.Copy(key, 0, parameterBlock, (2 + niv), nkey);
                byte[] encryptedParameterBlock = rsa.Encrypt(parameterBlock, true);

                // The encyphered Rinjadel parameter block are then
                // prefixed by its length (scrambled) and then written
                // to the buffer:

                Int32 nencryptedParameterBlock = (Int32)encryptedParameterBlock.Length;
                byte[] prefix = BitConverter.GetBytes((int)(nencryptedParameterBlock ^ 0xDEADBEEF));
                buffer.Write(prefix, 0, 4);
                buffer.Write(encryptedParameterBlock, 0, nencryptedParameterBlock);

                // Encrypt the bytes using Rinjadel:

                using (CryptoStream encryptor = new CryptoStream(buffer, rijndael.CreateEncryptor(), CryptoStreamMode.Write)) {
                    Int32 nbytes = (Int32)((bytes == null)? 0: bytes.Length);
                    encryptor.Write(BitConverter.GetBytes(nbytes), 0, 4);
                    encryptor.Write(bytes, 0, nbytes);
                    
                    encryptor.Flush();
                }

                encrypted = buffer.ToArray();
            }

            return encrypted;
        }
    }
}
