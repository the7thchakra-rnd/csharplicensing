﻿/**
 * LicenseCertificate.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using System;
using System.Collections.Generic;
using System.IO;
using System.Management;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Reflection;

namespace ZydecoSoftwareLLC.Licensing {

    /// <summary>
    /// Encapsulates a license certificate.
    /// </summary>
    [ComVisible(true)]
    [Serializable]
    public class LicenseCertificate: IBinarySerializable {

        internal LicenseCertificate(object obj) {
            foreach (FieldInfo src in obj.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance)) {
                FieldInfo dest = GetType().GetField(src.Name, BindingFlags.NonPublic | BindingFlags.Instance);
                object value = src.GetValue(obj);
                dest.SetValue(this, value);
            }
        }

        private Guid licenseRequestID;

        /// <summary>
        /// The GUID of the license request.
        /// </summary>
        public Guid LicenseRequestID {
            get { return licenseRequestID; }
        }

        private DateTime issuedDateTime = DateTime.UtcNow;

        /// <summary>
        /// The date when the certificate was issued.
        /// </summary>
        public DateTime IssuedDateTime {
            get { return issuedDateTime; }
        }

        /// <summary>
        /// Indicates if the license is locked to the requesting username.
        /// </summary>
        public bool IsUserNamedLocked { get; set; }

        /// <summary>
        /// Indicates if the license is locked to the requesting user's SID.
        /// </summary>
        public bool IsUserSIDLocked { get; set; }

        /// <summary>
        /// Indicates if the license is locked to
        /// the BIOS serial number(s) from the requesting machine.
        /// </summary>
        public bool IsBIOSSerialNumberLocked { get; set; }

        /// <summary>
        /// Indicates if the license is locked to
        /// the CPU ID(s) from the requesting machine.
        /// </summary>
        public bool IsCPUIDLocked { get; set; }

        /// <summary>
        /// Indicates if the license is locked to
        /// the MAC addresses from the requesting machine.
        /// </summary>
        public bool IsMACAddressesLocked { get; set; }

        /// <summary>
        /// Indicates if the license expires.
        /// </summary>
        public bool IsExpiring { get; set; }

        /// <summary>
        /// The expiration date of the license.
        /// </summary>
        public DateTime ExpiresDateTime { get; set; }

        /// <summary>
        /// Indicates if the license is locked to
        /// the major version of the licensing assembly.
        /// </summary>
        public bool IsMajorVersionLocked { get; set; }

        /// <summary>
        /// Indicates if the license is locked to
        /// the minor version of the licensing assembly.
        /// </summary>
        public bool IsMinorVersionLocked { get; set; }

        /// <summary>
        /// Indicates if the license is locked to
        /// the revision of the licensing assembly.
        /// </summary>
        public bool IsRevisionLocked { get; set; }

        /// <summary>
        /// Indicates if the license is locked to
        /// the build number of the licensing assembly.
        /// </summary>
        public bool IsBuildLocked { get; set; }

        /// <summary>
        /// Indicates if the license is for a server.
        /// </summary>
        public bool IsServerCertificate { get; set; }

        /// <summary>
        /// The maximum license lease duration (in minutes).
        /// </summary>
        public int MaximumLicenseLeaseDuration { get; set; }

        /// <summary>
        /// The maximum number of leases.
        /// </summary>
        public int MaximumLeases { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="request">the license request</param>
        public LicenseCertificate(LicenseRequest licenseRequest) {
            licenseRequestID = licenseRequest.LicenseRequestID;
        }

        #region Interface Implementations

        #region Interface Implementation :: ZydecoSoftwareLLC.Licensing.IBinarySerializable

        private LicenseCertificate(BinaryReader reader) {
            // issued
            Int32 issuedDateTimeTicks = reader.ReadInt32();
            issuedDateTime = new DateTime(issuedDateTimeTicks * TimeSpan.TicksPerDay + new DateTime(2010, 1, 1).Ticks);

            // licenseRequestID
            byte[] licenseRequestIDBytes = new Guid().ToByteArray();
            reader.Read(licenseRequestIDBytes, 0, 4);
            licenseRequestID = new Guid(licenseRequestIDBytes);

            UInt16 bits = reader.ReadUInt16();

            // IsUserNamedLocked
            IsUserNamedLocked = ((bits & 0x0001) != 0);

            // IsUserSIDLocked
            IsUserSIDLocked = ((bits & 0x0002) != 0);

            // IsBIOSSerialNumberLocked
            IsBIOSSerialNumberLocked = ((bits & 0x0004) != 0);

            // IsCPUIDLocked
            IsCPUIDLocked = ((bits & 0x0008) != 0);

            // IsMACAddressesLocked
            IsMACAddressesLocked = ((bits & 0x0010) != 0);

            // IsExpiring
            IsExpiring = ((bits & 0x0020) != 0);

            // IsMajorVersionLocked
            IsMajorVersionLocked = ((bits & 0x0040) != 0);

            // IsMinorVersionLocked
            IsMinorVersionLocked = ((bits & 0x0080) != 0);

            // IsRevisionLocked
            IsRevisionLocked = ((bits & 0x0100) != 0);

            // IsBuildLocked
            IsBuildLocked = ((bits & 0x0200) != 0);

            // ExpiresDateTime
            Int32 expiresDateTimeTicks = reader.ReadInt32();
            ExpiresDateTime = new DateTime(expiresDateTimeTicks * TimeSpan.TicksPerDay + new DateTime(2010, 1, 1).Ticks);

            // IsServerCertificate
            IsServerCertificate = ((bits & 0x0400) != 0);

            if (IsServerCertificate) {

                // MaximumLeases
                MaximumLeases = reader.ReadInt32();

                // MaximumLicenseLeaseDuration
                MaximumLicenseLeaseDuration = reader.ReadInt32();
            }
        }

        public void WriteTo(BinaryWriter writer) {
            // issued
            writer.Write((Int32)((issuedDateTime.Ticks - new DateTime(2010, 1, 1).Ticks) / TimeSpan.TicksPerDay));

            // licenseRequestID
            byte[] licenseRequestIDBytes = licenseRequestID.ToByteArray();
            writer.Write(licenseRequestIDBytes, 0, 4);

            // IsUserNamedLocked
            UInt16 bits = 0;
            bits |= (UInt16)(IsUserNamedLocked ? 0x0001 : 0);

            // IsUserSIDLocked
            bits |= (UInt16)(IsUserSIDLocked ? 0x0002 : 0);

            // IsBIOSSerialNumberLocked
            bits |= (UInt16)(IsBIOSSerialNumberLocked ? 0x0004 : 0);

            // IsCPUIDLocked
            bits |= (UInt16)(IsCPUIDLocked ? 0x0008 : 0);

            // IsMACAddressesLocked
            bits |= (UInt16)(IsMACAddressesLocked ? 0x0010 : 0);

            // IsExpiring
            bits |= (UInt16)(IsExpiring ? 0x0020 : 0);

            // IsMajorVersionLocked
            bits |= (UInt16)(IsMajorVersionLocked ? 0x0040 : 0);

            // IsMinorVersionLocked
            bits |= (UInt16)(IsMinorVersionLocked ? 0x0080 : 0);

            // IsRevisionLocked
            bits |= (UInt16)(IsRevisionLocked ? 0x0100 : 0);

            // IsBuildLocked
            bits |= (UInt16)(IsBuildLocked ? 0x0200 : 0);

            // IsServerCertificate
            bits |= (UInt16)(IsServerCertificate ? 0x0400 : 0);

            writer.Write(bits);

            // ExpiresDateTime
            writer.Write((Int32)((ExpiresDateTime.Ticks - new DateTime(2010, 1, 1).Ticks) / TimeSpan.TicksPerDay));

            // MaximumLeases
            writer.Write((Int32)MaximumLeases);

            // MaximumLicenseLeaseDuration
            writer.Write((Int32)MaximumLicenseLeaseDuration);
        }

        #endregion

        #endregion


        public override string ToString() {
            string value;
            StringBuilder buffer = new StringBuilder();

            buffer.Append("LicenseRequestID: ");
            buffer.Append(licenseRequestID);
            buffer.Append("\r\n");

            buffer.Append("IssuedDateTime: ");
            buffer.Append(issuedDateTime);
            buffer.Append("\r\n");

            if (IsUserNamedLocked) {
                buffer.Append("IsUserNameLocked: true\r\n");
            }

            if (IsUserSIDLocked) {
                buffer.Append("IsUserSIDLocked: true\r\n");
            }

            if (IsBIOSSerialNumberLocked) {
                buffer.Append("IsBIOSSerialNumberLocked: true\r\n");
            }

            if (IsCPUIDLocked) {
                buffer.Append("IsCPUIDLocked: true\r\n");
            }

            if (IsMACAddressesLocked) {
                buffer.Append("IsMACAddressesLocked: true\r\n");
            }

            if (IsMajorVersionLocked) {
                buffer.Append("IsMajorVersionLocked: true\r\n");
            }

            if (IsMinorVersionLocked) {
                buffer.Append("IsMinorVersionLocked: true\r\n");
            }

            if (IsRevisionLocked) {
                buffer.Append("IsRevisionLocked: true\r\n");
            }

            if (IsBuildLocked) {
                buffer.Append("IsBuildLocked: true\r\n");
            }

            if (IsExpiring) {
                buffer.Append("IsExpiring: true\r\n");
                buffer.Append(string.Format("ExpiresDateTime: {0:dddd, MMMM d, yyyy}\r\n", ExpiresDateTime));
            }

            value = buffer.ToString();

            return value;
        }
    }
}
