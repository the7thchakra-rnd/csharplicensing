﻿using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

namespace ZydecoSoftwareLLC.Licensing {

    /// <summary>
    /// Encrypts bytes.
    /// </summary>
    [ComVisible(true)]
    public interface IEncrypt {

        /// <summary>
        /// Encrypt the given bytes.
        /// </summary>
        /// <param name="bytes">the bytes to encrypt</param>
        /// <returns>the encrypted bytes</returns>
        byte[] Encrypt(byte[] bytes);
    }
}
