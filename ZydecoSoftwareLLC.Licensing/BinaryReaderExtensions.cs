﻿/**
 * BinaryReaderExtensions.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using System;
using System.IO;

namespace ZydecoSoftwareLLC.Licensing {

    /// <summary>
    /// Extensions to <see cref="BinaryReader"/>.
    /// </summary>
    internal static class BinaryReaderExtensions {

        /// <summary>
        /// Read a length-prefixed block of bytes.
        /// </summary>
        /// <param name="self">the <see cref="BinaryReader"/></param>
        /// <returns>the bytes</returns>
        internal static byte[] ReadBytes(this BinaryReader self) {
            byte[] value;

            if (!self.ReadBoolean()) {
                value = null;
            }
            else {
                int length = self.ReadInt32();
                value = new byte[length];
                self.Read(value, 0, length);
            }

            return value;
        }
    }
}
