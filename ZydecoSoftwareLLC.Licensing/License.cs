﻿/**
 * License.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Windows.Forms;
using System.Collections;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Security.AccessControl;

namespace ZydecoSoftwareLLC.Licensing {

    /// <summary>
    /// Encapsulates the license.
    /// </summary>
    /// <remarks>
    /// The license consists of these parts:
    /// 
    /// <list type="bullet">
    /// <item>license certificates and</item>
    /// <item>license requests</item>
    /// </list>
    /// </remarks>
    [ComVisible(true)]
    [Serializable]
    public class License {
        private const string REGISTRYKEY_LICENSES = "Software\\ZydecoSoftwareLLC, LLC\\Licenses";

        private const string REGISTRYKEY_SERVICE_LICENSES = "SYSTEM\\CurrentControlSet\\services\\ZydecoSoftwareLLC, LLC, Licensing Server\\Licenses";

        private static readonly License[] license = new License[1];

        private Dictionary<Guid, LicenseRequest> licenseRequestByLicenseRequestID = new Dictionary<Guid, LicenseRequest>();

        public static readonly IPAddress LICENSE_SERVER_MULTICAST_ADDRESS = IPAddress.Parse("224.3.5.72");

        public const int LICENSE_SERVER_MULTICAST_PORT = 3572;

        public const int LICENSE_SERVER_TCP_PORT = 3572;

        internal static LicenseServerAvailableMessage licenseServerAvailableMessage;

        private static bool licenseServerAvailableMessageLocked;

        private static DateTime licenseServerAvailableMessageReceived;

        private static Thread[] listenThread = new Thread[1];

        private static bool listenThreadEnabled = true;

        private List<Lease> leases = new List<Lease>();

        public static void DisableListenThread() {
            listenThreadEnabled = false;
        }

        public static string UserName { internal get; set; }

        internal static Lease lease;

        private static LicenseRequest leaseRequest;

        internal static bool noLeasesAvailable;

        private static Thread refreshLicenseThread;

        public static bool IsLicenseServerAvailable {
            get {
                return ((licenseServerAvailableMessage != null) && (licenseServerAvailableMessageReceived.AddSeconds(60) >= DateTime.UtcNow));
            }
        }

        public static bool? IsLeaseAvailable {
            get { return ((leaseRequest != null) ? (bool?)(lease != null) : null); }
        }

        public LicenseCertificate LicenseCertificate {
            get {
                LicenseCertificate value;
                LicenseRequest pendingLicenseRequest = licenseRequestByLicenseRequestID.Values.OrderByDescending(it => it.ExpiresDateTime).FirstOrDefault();
                if (pendingLicenseRequest == null) {
                    value = null;
                }
                else {
                    value = pendingLicenseRequest.LicenseCertificate;
                }

                return value;
            }
        }

        public static void PurgeLeases() {
            License instance = Instance;
            instance.leases.Clear();
            instance.Save();
        }

        /// <summary>
        /// The license.
        /// </summary>
        public static License Instance {
            get {
                StartListenThread();

                License value;
                if ((value = license[0]) == null) {
                    lock (license) {
                        if ((value = license[0]) == null) {
                            value = LoadLicense();
                        }
                    }
                }

                return value;
            }
        }

        public IList<Lease> Leases {
            get {
                if (leases == null) {
                    return new List<Lease>();
                }
                return new List<Lease>(leases);
            }
        }

        public Lease CreateLease(LicenseRequest request) {
            Lease lease;
            lock (license) {

                // Purge leases that will expire in 5 minutes.
                // This helps to prevent starvation.
                PurgeExpiredLeases(DateTime.UtcNow.AddMinutes(10));

                LicenseCertificate serverCertificate = LicenseCertificate;
                if ((serverCertificate == null) || !serverCertificate.IsServerCertificate) {
                    lease = null;
                }
                else {
                    if (leases == null) {
                        leases = new List<Lease>();
                    }

                    foreach (Lease candidate in leases) {
                        if (candidate.LicenseRequest.Equals(request)) {
                            candidate.Touch();

                            Save();

                            return candidate;
                        }
                    }

                    if (leases.Count >= serverCertificate.MaximumLeases) {
                        lease = null;
                    }
                    else {
                        LicenseCertificate certificate = new LicenseCertificate(request);
                        certificate.IsBIOSSerialNumberLocked = true;
                        certificate.IsBuildLocked = true;
                        certificate.IsCPUIDLocked = true;
                        certificate.IsExpiring = true;
                        certificate.ExpiresDateTime = certificate.IssuedDateTime.AddMinutes(serverCertificate.MaximumLicenseLeaseDuration);
                        certificate.IsMACAddressesLocked = true;
                        certificate.IsMajorVersionLocked = true;
                        certificate.IsMinorVersionLocked = true;
                        certificate.IsRevisionLocked = true;
                        certificate.IsUserNamedLocked = true;
                        certificate.IsUserSIDLocked = true;

                        lease = new Lease(request, certificate);

                        leases.Add(lease);

                        Save();
                    }
                }
            }

            return lease;
        }

        private void PurgeExpiredLeases(DateTime now) {
            IList<Lease> leases = Leases;
            foreach (Lease lease in leases) {
                if (lease.IsExpired(now)) {
                    this.leases.Remove(lease);
                }
            }

            Save();
        }

        public void DropLease(LicenseRequest request) {
            lock (license) {

                // Purge leases that will expire in 5 minutes.
                // This helps to prevent starvation.
                PurgeExpiredLeases(DateTime.UtcNow.AddMinutes(10));

                LicenseCertificate serverCertificate = LicenseCertificate;
                if ((serverCertificate == null) || !serverCertificate.IsServerCertificate) {
                    lease = null;
                }
                else {
                    if (leases == null) {
                        leases = new List<Lease>();
                    }

                    foreach (Lease candidate in leases) {
                        if (candidate.LicenseRequest.Equals(request)) {
                            leases.Remove(candidate);

                            Save();

                            return;
                        }
                    }
                }
            }
        }

        private License() {
        }

        private License(object obj) {
            IEnumerable licenseRequestByLicenseRequestID = (IEnumerable)obj.GetType().GetField("licenseRequestByLicenseRequestID",
                BindingFlags.NonPublic | BindingFlags.Instance).GetValue(obj);
            foreach (object entry in licenseRequestByLicenseRequestID) {
                Guid key = (Guid)entry.GetType().GetProperty("Key").GetValue(entry, null);
                object value = entry.GetType().GetProperty("Value").GetValue(entry, null);
                LicenseRequest licenseRequest = new LicenseRequest(value);
                this.licenseRequestByLicenseRequestID[key] = licenseRequest;
            }

            try {
                IEnumerable leases = (IEnumerable)obj.GetType().GetField("leases",
                    BindingFlags.NonPublic | BindingFlags.Instance).GetValue(obj);
                foreach (object lease in leases) {
                    Lease clone = new Lease(lease);
                    this.leases.Add(clone);
                }
            }
            catch {
            }
        }

        private static RegistryKey OpenLicensesKey(bool create) {
            RegistryKey licenses = Registry.CurrentUser.OpenSubKey(REGISTRYKEY_LICENSES, true);
            if ((licenses == null) && create) {
                licenses = Registry.CurrentUser.CreateSubKey(REGISTRYKEY_LICENSES);
            }

            return licenses;
        }

        private static object GetLicenseValue(string name) {
            lock (license) {
                if (listenThreadEnabled) {
                    RegistryKey licenses = OpenLicensesKey(true);
                    using (licenses) {
                        return licenses.GetValue(name);
                    }
                }
                else {
                    Assembly assembly = Assembly.GetEntryAssembly();
                    string assemblyPath = assembly.Location;
                    FileInfo assemblyFile = new FileInfo(assemblyPath);
                    string path = (assemblyFile.DirectoryName + "\\licensing.dat");

                    Dictionary<string, object> data;
                    try {
                        data = (Dictionary<string, object>)LicensingServices.ToObject(path);
                    }
                    catch {
                        data = new Dictionary<string, object>();
                    }

                    object value;
                    data.TryGetValue(name, out value);

                    return value;
                }
            }
        }

        private static void SetLicenseValue(string name, object value) {
            lock (license) {
                if (listenThreadEnabled) {
                    RegistryKey licenses = OpenLicensesKey(true);
                    using (licenses) {
                        licenses.SetValue(name, value);
                    }
                }
                else {
                    Assembly assembly = Assembly.GetEntryAssembly();
                    string assemblyPath = assembly.Location;
                    FileInfo assemblyFile = new FileInfo(assemblyPath);
                    string path = (assemblyFile.DirectoryName + "\\licensing.dat");

                    Dictionary<string, object> data;
                    try {
                        data = (Dictionary<string, object>)LicensingServices.ToObject(path);
                    }
                    catch {
                        data = new Dictionary<string, object>();
                    }

                    data[name] = value;

                    LicensingServices.Serialize(data, path);

                    try {
                        FileSystemAccessRule everyOne = new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.None, PropagationFlags.None, AccessControlType.Allow);
                        FileSecurity fileSecurity = new FileSecurity(path, AccessControlSections.Group);
                        fileSecurity.AddAccessRule(everyOne);
                        File.SetAccessControl(path, fileSecurity);
                    }
                    catch (Exception cause) {
                        Console.WriteLine(cause);
                    }
                }
            }
        }

        /// <summary>
        /// Load the license from the F
        /// </summary>
        /// <returns>the license</returns>
        private static License LoadLicense() {
            License license;

            Log("Loading license.");

            lock (License.license) {
                string token = (GetLicenseValue(string.Empty) as string);
                if (string.IsNullOrEmpty(token)) {
                    license = new License();

                    // license.Save();
                }
                else {
                    object obj = CryptographyServices.GetFromRawToken<object>(token);
                    license = new License(obj);
                }
            }

            return license;
        }

        /// <summary>
        /// Ensure the the current user is licensed 
        /// to perform the given method.
        /// </summary>
        /// <param name="method"></param>
        internal void EnsureIsLicensed(bool server, bool showInfoDialog, MethodBase method) {
            for (int retry = 0; retry < 2; retry += 1) {
                License current = License.Instance;

                string reason = null;
                IEnumerable<LicenseRequest> licenseRequests = current.licenseRequestByLicenseRequestID.Values.Where(it => !it.IsPending).OrderBy(it => (it.LicenseCertificate.IsExpiring ? it.LicenseCertificate.ExpiresDateTime : it.LicenseCertificate.IssuedDateTime));

                Log("Licenses:{0}", licenseRequests.Select(it => it.ToString()).Join(",\n  ", prefix:"\n  ", suffix:"."));

                if (licenseRequests.Count() == 0) {
                    reason = "You have no licenses as this time.";
                }
                else {
                    foreach (LicenseRequest licenseRequest in licenseRequests) {
                        reason = licenseRequest.TryEnsureIsLicensed(server, method);
                    }
                }

                if (reason == null) {
                    break;
                }
                else if ((retry == 0) && showInfoDialog) {
                    RequestLicenseForm requestLicenseForm = new RequestLicenseForm();
                    requestLicenseForm.Reason = reason;
                    requestLicenseForm.ShowDialog();
                }
                else {
                    throw new LicenseException(reason);
                }
            }
        }

        internal string GetBriefInfo(bool server, out bool valid) {
            string reason;
            if (lease != null) {
                DateTime latestExpirationDateTime = new DateTime(0);
                leaseRequest.GetBriefInfo(ref latestExpirationDateTime);

                DateTime now = DateTime.UtcNow;
                if (latestExpirationDateTime.Ticks <= 0) {
                    reason = "You have a non-expiring lease.";
                    valid = true;

                    Log("(B) valid: {0}, reason: {1}", valid, reason);
                }
                else {
                    TimeSpan diff = (now - latestExpirationDateTime);
                    if (diff.Ticks >= 0) {
                        reason = string.Format("Your licenses have expired.");
                        valid = false;

                        Log("(C) valid: {0}, reason: {1}", valid, reason);
                    }
                    else if (((int)diff.Ticks / TimeSpan.TicksPerDay) >= -60) {
                        reason = leaseRequest.TryEnsureIsLicensed(server, null);

                        if (reason != null) {
                            valid = false;

                            Log("(D) valid: {0}, reason: {1}", valid, reason);
                        }
                        else {
                            valid = true;
                            int days = -(int)diff.TotalDays;
                            if (days <= 0) {
                                reason = string.Format("The license will expire on {0:dddd, MMMM d, yyyy}.  You have {1} minute{2} remaining.",
                                    latestExpirationDateTime,
                                    -(int)diff.TotalMinutes,
                                    ((((int)diff.TotalMinutes) == 1) ? "" : "s"));
                            }
                            else {
                                reason = string.Format("The license will expire on {0:dddd, MMMM d, yyyy}.  You have {1} day{2} remaining.",
                                    latestExpirationDateTime,
                                    days,
                                    ((((int)diff.TotalDays) == 1) ? "" : "s"));
                            }

                            Log("(E) valid: {0}, reason: {1}", valid, reason);
                        }
                    }
                    else {
                        reason = null;
                        valid = true;

                        Log("(F) valid: {0}, reason: {1}", valid, reason);
                    }
                }
            }
            else {
                IEnumerable<LicenseRequest> licenseRequests = licenseRequestByLicenseRequestID.Values.Where(it => !it.IsPending).OrderBy(it => (it.LicenseCertificate.IsExpiring ? it.LicenseCertificate.ExpiresDateTime : it.LicenseCertificate.IssuedDateTime));
                if (licenseRequests.Count() == 0) {
                    reason = "You have no licenses as this time.";
                    valid = false;

                    Log("(A) valid: {0}, reason: {1}", valid, reason);
                }
                else {
                    DateTime latestExpirationDateTime = new DateTime(0);
                    foreach (LicenseRequest licenseRequest in licenseRequests) {
                        licenseRequest.GetBriefInfo(ref latestExpirationDateTime);
                    }

                    DateTime now = DateTime.UtcNow;
                    if (latestExpirationDateTime.Ticks <= 0) {
                        reason = "You have a non-expiring license.";
                        valid = true;

                        Log("(B) valid: {0}, reason: {1}", valid, reason);
                    }
                    else {
                        TimeSpan diff = (now - latestExpirationDateTime);
                        if (diff.Ticks >= 0) {
                            reason = string.Format("Your licenses have expired.");
                            valid = false;

                            Log("(C) valid: {0}, reason: {1}", valid, reason);
                        }
                        else if (((int)diff.Ticks / TimeSpan.TicksPerDay) >= -60) {
                            reason = null;
                            foreach (LicenseRequest licenseRequest in licenseRequests) {
                                reason = licenseRequest.TryEnsureIsLicensed(server, null);
                            }

                            if (reason != null) {
                                valid = false;

                                Log("(D) valid: {0}, reason: {1}", valid, reason);
                            }
                            else {
                                valid = true;
                                int days = -(int)diff.TotalDays;
                                if (days <= 0) {
                                    reason = string.Format("The license will expire on {0:dddd, MMMM d, yyyy}.  You have {1} minute{2} remaining.",
                                        latestExpirationDateTime,
                                        -(int)diff.TotalMinutes,
                                        ((((int)diff.TotalMinutes) == 1) ? "" : "s"));
                                }
                                else {
                                    reason = string.Format("The license will expire on {0:dddd, MMMM d, yyyy}.  You have {1} day{2} remaining.",
                                        latestExpirationDateTime,
                                        days,
                                        ((((int)diff.TotalDays) == 1) ? "" : "s"));
                                }

                                Log("(E) valid: {0}, reason: {1}", valid, reason);
                            }
                        }
                        else {
                            reason = null;
                            valid = true;

                            Log("(F) valid: {0}, reason: {1}", valid, reason);
                        }
                    }
                }
            }

            return reason;
        }

        internal void ApplyLicense(LicenseCertificate licenseCertificate) {
            Log("Applying license {0}.", licenseCertificate);

            if (licenseCertificate == null) {
                LicenseException exception = new LicenseException("The license is invalid.");
                throw exception;
            }

            LicenseRequest pendingLicenseRequest = licenseRequestByLicenseRequestID.Values.FirstOrDefault(it => (CompareLicenseID(it.LicenseRequestID, licenseCertificate.LicenseRequestID) == 0));

            Log("Matched license request ({0}): {1}.",
                licenseCertificate.LicenseRequestID,
                pendingLicenseRequest);

            if (pendingLicenseRequest == null) {
                LicenseException exception = new LicenseException("There is no pending license request.");
                throw exception;
            }

            pendingLicenseRequest.LicenseCertificate = licenseCertificate;

            licenseRequestByLicenseRequestID.Clear();
            licenseRequestByLicenseRequestID[pendingLicenseRequest.LicenseRequestID] = pendingLicenseRequest;

            Save();
        }

        private int CompareLicenseID(Guid lhs, Guid rhs) {
            byte[] lhsBytes = lhs.ToByteArray();
            byte[] rhsBytes = rhs.ToByteArray();

            int delta = 0;
            for (int i = 0; i < 4; i += 1) {
                delta = (lhsBytes[i] - rhsBytes[i]);
                if (delta != 0) {
                    break;
                }
            }

            return delta;
        }

        /// <summary>
        /// Store the license in the registry.
        /// </summary>
        internal void Save() {
            Log("Saving licenses.");

            string token = CryptographyServices.ToRawToken(this);
            SetLicenseValue(string.Empty, token);
        }

        /// <summary>
        /// Create a license request.
        /// </summary>
        /// <remarks>
        /// If there's an existing license request, the requested date is updated.
        /// </remarks>
        /// <returns>the license request</returns>
        internal LicenseRequest NewLicenseRequest() {

            // Drop older license requests:
            while (licenseRequestByLicenseRequestID.Count >= 5) {
                licenseRequestByLicenseRequestID.Remove(licenseRequestByLicenseRequestID.Values.
                    OrderBy(it => it.LicenseRequestedDateTime).
                    First(it => true).LicenseRequestID);
            }

            // Find the pending license request (if there is one).
            // If there isn't a pending license request, create one.
            LicenseRequest pendingLicenseRequest = new LicenseRequest();
            licenseRequestByLicenseRequestID[pendingLicenseRequest.LicenseRequestID] = pendingLicenseRequest;

            Log("Added license request {0}.\nQueued licenses request:{1}.",
                pendingLicenseRequest.LicenseRequestID,
                licenseRequestByLicenseRequestID.Values.Select(it => it.LicenseRequestID.ToString()).Join(",\n  ", prefix:"\n  ",suffix:"."));

            // Update the requested date of the pending license request.
            pendingLicenseRequest.LicenseRequestedDateTime = DateTime.UtcNow;

            // Save the license.
            Save();

            return pendingLicenseRequest;
        }


        internal bool TryGetLicenseCertificate(string text, out string licenseCertificateText, out LicenseCertificate licenseCertificate) {
            bool found = false;
            licenseCertificateText = null;
            licenseCertificate = null;
            foreach (LicenseRequest licenseRequest in licenseRequestByLicenseRequestID.Values.Where(it => it.IsPending)) {
                try {
                    found = licenseRequest.TryGetJacketResponseToken<LicenseCertificate>(text, out licenseCertificateText, out licenseCertificate);
                    if (found) {
                        break;
                    }
                }
                catch {
                    found = false;
                    licenseCertificateText = null;
                    licenseCertificate = null;
                }
            }

            return found;
        }

        private static readonly object logLock = new object();

        public static void Log(string message, params object[] arguments) {
            try {
                lock (logLock) {
                    string path = (Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ZydecoSoftwareLLC, LLC");
                    if (!Directory.Exists(path)) {
                        Directory.CreateDirectory(path);
                    }
                    FileInfo fileInfo = new FileInfo(path + @"\log.txt");
                    if (!string.IsNullOrEmpty(path)) {
                        if (fileInfo.Exists && (fileInfo.Length > 1024 * 1024)) {
                            RollLogs(path);
                        }
                        File.AppendAllText((path + @"\log.txt"), string.Format(("===== " + DateTime.UtcNow + "\r\n" + message + "\r\n"), arguments));
                    }
                }
            }
            catch {
            }
        }

        private static void RollLogs(string folder) {
            MoveLog(folder, "log4.txt", "log5.txt");
            MoveLog(folder, "log3.txt", "log4.txt");
            MoveLog(folder, "log2.txt", "log3.txt");
            MoveLog(folder, "log1.txt", "log2.txt");
            MoveLog(folder, "log.txt", "log1.txt");
        }

        private static void MoveLog(string folder, string from, string to) {
            if (File.Exists(folder + @"\" + from)) {
                if (File.Exists(folder + @"\" + to)) {
                    File.Delete(folder + @"\" + to);
                }

                File.Move(folder + @"\" + from, folder + @"\" + to);
            }
        }

        public static void StartListenThread() {
            lock (listenThread) {
                if (listenThreadEnabled) {
                    if (listenThread[0] == null) {
                        listenThread[0] = new Thread(RunListenThread);
                        listenThread[0].IsBackground = true;
                        listenThread[0].Name = "Listen";
                        listenThread[0].Start();
                    }

                    if (refreshLicenseThread == null) {
                        refreshLicenseThread = new Thread(RefreshLicense);
                        refreshLicenseThread.IsBackground = true;
                        refreshLicenseThread.Start();
                    }

                    if (licenseServerAvailableMessage == null) {
                        string address;
                        IPEndPoint tcpEndPoint = GetLicenseServerTCPEndPoint(out address);
                        if ((tcpEndPoint != null) && !IPAddress.Any.Equals(tcpEndPoint.Address)) {
                            licenseServerAvailableMessage = new LicenseServerAvailableMessage(tcpEndPoint);
                            licenseServerAvailableMessageLocked = true;
                            licenseServerAvailableMessageReceived = DateTime.UtcNow;
                        }
                    }
                }
            }
        }

        private static void RunListenThread() {
            for (; ; ) {
                try {
                    using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)) {
                        string udpAddress;
                        socket.Bind(new IPEndPoint(IPAddress.Any, GetLicenseServerMulticastEndPoint(out udpAddress).Port));
                        socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(GetLicenseServerMulticastEndPoint(out udpAddress).Address));

                        byte[] data = new byte[32768];
                        for (; ; ) {
                            EndPoint serverEndPoint = new IPEndPoint(IPAddress.Any, GetLicenseServerMulticastEndPoint(out udpAddress).Port);
                            int n = socket.ReceiveFrom(data, ref serverEndPoint);

                            object message = LicensingServices.ToObject(data, 0, n);

                            ThreadPool.QueueUserWorkItem(ProcessRequest, new Object[] { serverEndPoint, message });
                        }
                    }
                }
                catch (ThreadInterruptedException cause) {
                    Log("unexpected exception: {0}", cause);

                    break;
                }
                catch (ThreadAbortException cause) {
                    Log("unexpected exception: {0}", cause);
                    
                    break;
                }
                catch (Exception cause) {
                    Log("unexpected exception: {0}", cause);

                    Thread.Sleep(10000);
                }
            }
        }

        public static IPEndPoint GetLicenseServerMulticastEndPoint(out string token) {
            lock (license) {
                token = (GetLicenseValue("udp") as string);
                if (string.IsNullOrEmpty(token)) {
                    return new IPEndPoint(LICENSE_SERVER_MULTICAST_ADDRESS, LICENSE_SERVER_MULTICAST_PORT);
                }

                int indexOfColon = token.LastIndexOf(':');
                string address = token.Substring(0, indexOfColon);
                int port = int.Parse(token.Substring(indexOfColon + 1));

                if ("*".Equals(address)) {
                    return new IPEndPoint(IPAddress.Any, port);
                }
                else {
                    return new IPEndPoint(ParseIPAddress(address), port);
                }
            }
        }

        public static IPEndPoint GetLicenseServerTCPEndPoint(out string token) {
            lock (license) {
                try {
                    token = (GetLicenseValue("tcp") as string);
                    if (string.IsNullOrEmpty(token)) {
                        return null;
                    }

                    int indexOfColon = token.LastIndexOf(':');
                    string address = token.Substring(0, indexOfColon);
                    int port = int.Parse(token.Substring(indexOfColon + 1));

                    if ("*".Equals(address)) {
                        return new IPEndPoint(IPAddress.Any, port);
                    }
                    else {
                        return new IPEndPoint(ParseIPAddress(address), port);
                    }
                }
                catch (Exception) {
                    token = null;
                    return null;
                }
            }
        }

        public static void SetLicenseServerTCPEndPoint(string token) {
            SetLicenseValue("tcp", token);

            if (!string.IsNullOrEmpty(token)) {
                try {
                    licenseServerAvailableMessage = new LicenseServerAvailableMessage(GetLicenseServerTCPEndPoint(out token));
                    licenseServerAvailableMessageLocked = true;
                    licenseServerAvailableMessageReceived = DateTime.UtcNow;
                }
                catch {
                    licenseServerAvailableMessageLocked = false;
                    licenseServerAvailableMessageReceived = new DateTime(0);
                }
            }
            else {
                licenseServerAvailableMessageLocked = false;
                licenseServerAvailableMessageReceived = new DateTime(0);
            }
        }

        public static IPEndPoint GetLicenseTCPEndPoint(out string token) {
            lock (license) {
                token = (GetLicenseValue("tcp.server") as string);
                if (string.IsNullOrEmpty(token)) {
                    return null;
                }

                int indexOfColon = token.LastIndexOf(':');
                string address = token.Substring(0, indexOfColon);
                int port = int.Parse(token.Substring(indexOfColon + 1));

                if ("*".Equals(address)) {
                    return new IPEndPoint(IPAddress.Any, port);
                }
                else {
                    return new IPEndPoint(ParseIPAddress(address), port);
                }
            }
        }

        private static IPAddress ParseIPAddress(string address) {
            try {
                return IPAddress.Parse(address);
            }
            catch {
                return Dns.GetHostEntry(address).AddressList[0];
            }
        }

        private static void ProcessRequest(object arg) {
            object message = ((object[])arg)[1];
            Log("processing {0}", message);
            if (message is LicenseServerAvailableMessage) {
                if (!licenseServerAvailableMessageLocked) {
                    licenseServerAvailableMessage = (LicenseServerAvailableMessage)message;
                    licenseServerAvailableMessageReceived = DateTime.UtcNow;
                    licenseServerAvailableMessage.Address = ((IPEndPoint)((object[])arg)[0]).Address;
                }
            }
        }

        private static void RefreshLicense() {
            try {
                IPEndPoint lastServerEndPoint;
                for (; ; ) {
                    IPEndPoint serverEndPoint;
                    if (licenseServerAvailableMessage == null) {
                        serverEndPoint = null;
                    }
                    else {
                        serverEndPoint = new IPEndPoint(licenseServerAvailableMessage.Address, licenseServerAvailableMessage.Port);
                    }

                    if (serverEndPoint == null) {
                        Thread.Sleep(1000);
                        continue;
                    }

                    lastServerEndPoint = serverEndPoint;

                    try {
                        using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)) {
                            socket.Connect(new IPEndPoint(serverEndPoint.Address, licenseServerAvailableMessage.Port));
                            using (NetworkStream stream = new NetworkStream(socket)) {
                                for (; ; ) {
                                    LicensingServices.Serialize(leaseRequest = new LicenseRequest(), stream);

                                    object response = LicensingServices.ToObject(stream);
                                    if ("no leases available".Equals(response)) {
                                        noLeasesAvailable = true;
                                    }
                                    else if (response is Lease) {
                                        noLeasesAvailable = false;
                                        lease = (Lease)response;

                                        leaseRequest.licenseCertificate = lease.LicenseCertificate;
                                    }

                                    Thread.Sleep(10000);

                                    if (licenseServerAvailableMessage == null) {
                                        serverEndPoint = null;
                                    }
                                    else {
                                        serverEndPoint = new IPEndPoint(licenseServerAvailableMessage.Address, licenseServerAvailableMessage.Port);
                                    }

                                    if (!lastServerEndPoint.Equals(serverEndPoint)) {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception cause) {
                        Console.WriteLine(cause);
                        Log("unexpected exception: {0}", cause);
                    }
                }
            }
            catch (Exception cause) {
                Console.WriteLine(cause);
                Log("unexpected exception: {0}", cause);
            }
        }
    }
}
