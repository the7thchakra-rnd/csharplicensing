﻿namespace ZydecoSoftwareLLC.Licensing {
    partial class RequestLicenseForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RequestLicenseForm));
            this.copyLicenseRequestToClipboardAction = new System.Windows.Forms.LinkLabel();
            this.ok = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.step0 = new System.Windows.Forms.Label();
            this.step1 = new System.Windows.Forms.Label();
            this.step1d = new System.Windows.Forms.Label();
            this.requestLicense = new System.Windows.Forms.LinkLabel();
            this.mailtoAddress = new System.Windows.Forms.TextBox();
            this.step1b = new System.Windows.Forms.Label();
            this.step1c = new System.Windows.Forms.Label();
            this.sendEmail = new System.Windows.Forms.LinkLabel();
            this.step2 = new System.Windows.Forms.Label();
            this.step2a = new System.Windows.Forms.Label();
            this.applyLicense = new System.Windows.Forms.LinkLabel();
            this.step3 = new System.Windows.Forms.Label();
            this.reason = new System.Windows.Forms.TextBox();
            this.licenseDetails = new System.Windows.Forms.TextBox();
            this.clipboardContents = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // copyLicenseRequestToClipboardAction
            // 
            this.copyLicenseRequestToClipboardAction.AutoSize = true;
            this.copyLicenseRequestToClipboardAction.Location = new System.Drawing.Point(298, 171);
            this.copyLicenseRequestToClipboardAction.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.copyLicenseRequestToClipboardAction.Name = "copyLicenseRequestToClipboardAction";
            this.copyLicenseRequestToClipboardAction.Size = new System.Drawing.Size(400, 25);
            this.copyLicenseRequestToClipboardAction.TabIndex = 3;
            this.copyLicenseRequestToClipboardAction.TabStop = true;
            this.copyLicenseRequestToClipboardAction.Text = "copy the license request to the clipboard";
            this.copyLicenseRequestToClipboardAction.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.copyLicenseRequestToClipboardAction_LinkClicked);
            // 
            // ok
            // 
            this.ok.Location = new System.Drawing.Point(1394, 362);
            this.ok.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(150, 44);
            this.ok.TabIndex = 4;
            this.ok.Text = "&OK";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(441, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "A valid license is required to use this product";
            // 
            // step0
            // 
            this.step0.AutoSize = true;
            this.step0.Location = new System.Drawing.Point(24, 98);
            this.step0.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step0.Name = "step0";
            this.step0.Size = new System.Drawing.Size(391, 25);
            this.step0.TabIndex = 6;
            this.step0.Text = "Follow these steps to request a license:";
            // 
            // step1
            // 
            this.step1.AutoSize = true;
            this.step1.Location = new System.Drawing.Point(26, 135);
            this.step1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step1.Name = "step1";
            this.step1.Size = new System.Drawing.Size(31, 25);
            this.step1.TabIndex = 13;
            this.step1.Text = "1)";
            // 
            // step1d
            // 
            this.step1d.AutoSize = true;
            this.step1d.Location = new System.Drawing.Point(930, 171);
            this.step1d.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step1d.Name = "step1d";
            this.step1d.Size = new System.Drawing.Size(30, 25);
            this.step1d.TabIndex = 12;
            this.step1d.Text = "to";
            // 
            // requestLicense
            // 
            this.requestLicense.AutoSize = true;
            this.requestLicense.Location = new System.Drawing.Point(70, 135);
            this.requestLicense.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.requestLicense.Name = "requestLicense";
            this.requestLicense.Size = new System.Drawing.Size(301, 25);
            this.requestLicense.TabIndex = 14;
            this.requestLicense.TabStop = true;
            this.requestLicense.Text = "Request the license by e-mail.";
            this.requestLicense.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.requestLicense_LinkClicked);
            // 
            // mailtoAddress
            // 
            this.mailtoAddress.BackColor = System.Drawing.SystemColors.Control;
            this.mailtoAddress.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.mailtoAddress.Location = new System.Drawing.Point(974, 171);
            this.mailtoAddress.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.mailtoAddress.Name = "mailtoAddress";
            this.mailtoAddress.ReadOnly = true;
            this.mailtoAddress.Size = new System.Drawing.Size(570, 24);
            this.mailtoAddress.TabIndex = 22;
            // 
            // step1b
            // 
            this.step1b.AutoSize = true;
            this.step1b.Location = new System.Drawing.Point(70, 171);
            this.step1b.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step1b.Name = "step1b";
            this.step1b.Size = new System.Drawing.Size(218, 25);
            this.step1b.TabIndex = 23;
            this.step1b.Text = "Alternatively, you can";
            // 
            // step1c
            // 
            this.step1c.AutoSize = true;
            this.step1c.Location = new System.Drawing.Point(706, 171);
            this.step1c.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step1c.Name = "step1c";
            this.step1c.Size = new System.Drawing.Size(48, 25);
            this.step1c.TabIndex = 24;
            this.step1c.Text = "and";
            // 
            // sendEmail
            // 
            this.sendEmail.AutoSize = true;
            this.sendEmail.Location = new System.Drawing.Point(768, 171);
            this.sendEmail.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.sendEmail.Name = "sendEmail";
            this.sendEmail.Size = new System.Drawing.Size(153, 25);
            this.sendEmail.TabIndex = 25;
            this.sendEmail.TabStop = true;
            this.sendEmail.Text = "send an e-mail";
            this.sendEmail.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.sendEmail_LinkClicked);
            // 
            // step2
            // 
            this.step2.AutoSize = true;
            this.step2.Location = new System.Drawing.Point(26, 208);
            this.step2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step2.Name = "step2";
            this.step2.Size = new System.Drawing.Size(31, 25);
            this.step2.TabIndex = 27;
            this.step2.Text = "2)";
            // 
            // step2a
            // 
            this.step2a.AutoSize = true;
            this.step2a.Location = new System.Drawing.Point(70, 208);
            this.step2a.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step2a.Name = "step2a";
            this.step2a.Size = new System.Drawing.Size(606, 25);
            this.step2a.TabIndex = 29;
            this.step2a.Text = "Enter the license below or copy the license onto the clipboard.";
            // 
            // applyLicense
            // 
            this.applyLicense.AutoSize = true;
            this.applyLicense.Location = new System.Drawing.Point(70, 331);
            this.applyLicense.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.applyLicense.Name = "applyLicense";
            this.applyLicense.Size = new System.Drawing.Size(182, 25);
            this.applyLicense.TabIndex = 30;
            this.applyLicense.TabStop = true;
            this.applyLicense.Text = "Apply the license.";
            this.applyLicense.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.applyLicense_LinkClicked);
            // 
            // step3
            // 
            this.step3.AutoSize = true;
            this.step3.Location = new System.Drawing.Point(26, 331);
            this.step3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.step3.Name = "step3";
            this.step3.Size = new System.Drawing.Size(31, 25);
            this.step3.TabIndex = 31;
            this.step3.Text = "3)";
            // 
            // reason
            // 
            this.reason.BackColor = System.Drawing.SystemColors.Control;
            this.reason.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reason.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reason.Location = new System.Drawing.Point(56, 62);
            this.reason.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.reason.Name = "reason";
            this.reason.ReadOnly = true;
            this.reason.Size = new System.Drawing.Size(1488, 25);
            this.reason.TabIndex = 32;
            // 
            // licenseDetails
            // 
            this.licenseDetails.BackColor = System.Drawing.SystemColors.Control;
            this.licenseDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.licenseDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.licenseDetails.Location = new System.Drawing.Point(108, 288);
            this.licenseDetails.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.licenseDetails.Name = "licenseDetails";
            this.licenseDetails.ReadOnly = true;
            this.licenseDetails.Size = new System.Drawing.Size(1436, 25);
            this.licenseDetails.TabIndex = 34;
            // 
            // clipboardContents
            // 
            this.clipboardContents.Location = new System.Drawing.Point(108, 238);
            this.clipboardContents.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.clipboardContents.Name = "clipboardContents";
            this.clipboardContents.Size = new System.Drawing.Size(1432, 31);
            this.clipboardContents.TabIndex = 35;
            this.clipboardContents.TextChanged += new System.EventHandler(this.clipboardContents_TextChanged);
            // 
            // RequestLicenseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1568, 431);
            this.Controls.Add(this.clipboardContents);
            this.Controls.Add(this.licenseDetails);
            this.Controls.Add(this.reason);
            this.Controls.Add(this.step3);
            this.Controls.Add(this.applyLicense);
            this.Controls.Add(this.step2a);
            this.Controls.Add(this.step2);
            this.Controls.Add(this.sendEmail);
            this.Controls.Add(this.step1c);
            this.Controls.Add(this.step1b);
            this.Controls.Add(this.mailtoAddress);
            this.Controls.Add(this.requestLicense);
            this.Controls.Add(this.step1);
            this.Controls.Add(this.step1d);
            this.Controls.Add(this.step0);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ok);
            this.Controls.Add(this.copyLicenseRequestToClipboardAction);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RequestLicenseForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "ZydecoSoftwareLLC, LLC :: License Required";
            this.Load += new System.EventHandler(this.RequestLicenseForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel copyLicenseRequestToClipboardAction;
        private System.Windows.Forms.Button ok;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label step0;
        private System.Windows.Forms.Label step1;
        private System.Windows.Forms.Label step1d;
        private System.Windows.Forms.LinkLabel requestLicense;
        private System.Windows.Forms.TextBox mailtoAddress;
        private System.Windows.Forms.Label step1b;
        private System.Windows.Forms.Label step1c;
        private System.Windows.Forms.LinkLabel sendEmail;
        private System.Windows.Forms.Label step2;
        private System.Windows.Forms.Label step2a;
        private System.Windows.Forms.LinkLabel applyLicense;
        private System.Windows.Forms.Label step3;
        private System.Windows.Forms.TextBox reason;
        private System.Windows.Forms.TextBox licenseDetails;
        private System.Windows.Forms.TextBox clipboardContents;
    }
}
