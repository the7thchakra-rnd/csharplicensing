﻿/**
 * ClipboardAwareForm.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ZydecoSoftwareLLC.Licensing {

    /// <summary>
    /// Base class for forms that need to be aware
    /// of changes to the clipboard.
    /// </summary>
    public class ClipboardAwareForm: Form {
        private IntPtr nextClipboardViewer;

        [DllImport("User32.dll")]
        protected static extern IntPtr SetClipboardViewer(IntPtr hwnd);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool ChangeClipboardChain(IntPtr hwnd, IntPtr nextHWND);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam);

        protected ClipboardAwareForm() {
            nextClipboardViewer = SetClipboardViewer(this.Handle);
        }

        protected override void WndProc(ref Message message) {
            const int WM_DRAWCLIPBOARD = 0x308;
            const int WM_CHANGECBCHAIN = 0x030D;

            switch (message.Msg) {
            case WM_CHANGECBCHAIN:
                ChangeClipboardChain(ref message);
                break;

            case WM_DRAWCLIPBOARD:
                DrawClipboard(ref message);
                break;

            default:
                base.WndProc(ref message);
                break;
            }
        }

        private void DrawClipboard(ref Message message) {
            try {
                DrawClipboard();
            }
            finally {
                SendMessage(nextClipboardViewer, message.Msg, message.WParam, message.LParam);
            }
        }

        protected virtual void DrawClipboard() {
        }

        private void ChangeClipboardChain(ref Message message) {
            if (message.WParam == nextClipboardViewer) {
                nextClipboardViewer = message.LParam;
            }
            else {
                SendMessage(nextClipboardViewer, message.Msg, message.WParam, message.LParam);
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                ChangeClipboardChain(this.Handle, nextClipboardViewer);
            }

            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClipboardAwareForm));
            this.SuspendLayout();
            // 
            // ClipboardAwareForm
            // 
            this.ClientSize = new System.Drawing.Size(274, 229);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ClipboardAwareForm";
            this.ResumeLayout(false);

        }
    }
}
