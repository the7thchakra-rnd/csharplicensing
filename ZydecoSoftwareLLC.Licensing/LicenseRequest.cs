﻿/**
 * LicenseRequest.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Collections;

namespace ZydecoSoftwareLLC.Licensing {

    /// <summary>
    /// Encapsulates a license request.
    /// </summary>
    [ComVisible(true)]
    [Serializable]
    public class LicenseRequest: IBinarySerializable {
        private byte[] rijndaelIV;
        private byte[] rijndaelKey;

        private readonly Guid licenseRequestID;

        internal LicenseRequest(object obj) {
            foreach (FieldInfo src in obj.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance)) {
                FieldInfo dest = GetType().GetField(src.Name, BindingFlags.NonPublic | BindingFlags.Instance);
                object value = src.GetValue(obj);
                if ((dest.Name == "licenseCertificate") && (value != null)) {
                    value = new LicenseCertificate(value);
                }
                dest.SetValue(this, value);
            }
        }

        /// <summary>
        /// The GUID of the license request.
        /// </summary>
        public Guid LicenseRequestID {
            get { return licenseRequestID; }
        }

        private DateTime licenseRequestedDateTime;

        /// <summary>
        /// The date and time when the license request was created.
        /// </summary>
        public DateTime LicenseRequestedDateTime {
            get { return licenseRequestedDateTime; }
            internal set { licenseRequestedDateTime = value; }
        }

        private string userName;

        /// <summary>
        /// The requesting username.
        /// </summary>
        public string UserName {
            get { return userName; }
        }

        private string userSID;

        /// <summary>
        /// The requesting user's SID.
        /// </summary>
        public string UserSID {
            get { return userSID; }
        }

        private string biosSerialNumber;

        /// <summary>
        /// The BIOS serial number(s) from the requesting machine.
        /// </summary>
        public string BIOSSerialNumber {
            get { return biosSerialNumber; }
        }

        private string cpuID;

        /// <summary>
        /// The CPU ID(s) from the requesting machine.
        /// </summary>
        public string CPUID {
            get { return cpuID; }
        }

        private string macAddresses;

        /// <summary>
        /// The MAC addresses from the requesting machine.
        /// </summary>
        public string MACAddresses {
            get { return macAddresses; }
        }

        private string version;

        /// <summary>
        /// The version of the licensing assembly.
        /// </summary>
        public string Version {
            get { return version; }
        }

        internal LicenseCertificate licenseCertificate;

        /// <summary>
        /// Indicates if the license request is pending.
        /// </summary>
        internal bool IsPending {
            get { return (licenseCertificate == null); }
        }

        /// <summary>
        /// The license certificate.
        /// </summary>
        internal LicenseCertificate LicenseCertificate {
            get { return licenseCertificate; }
            set { licenseCertificate = value; }
        }

        internal DateTime ExpiresDateTime {
            get {
                DateTime value;
                if (licenseCertificate == null) {
                    value = new DateTime(0);
                }
                else if (!licenseCertificate.IsExpiring) {
                    value = new DateTime(long.MaxValue);
                }
                else {
                    value = licenseCertificate.ExpiresDateTime;
                }

                return value;
            }
        }

        /// <summary>
        /// Get a request token for this license request.
        /// </summary>
        /// <returns></returns>
        public string GetRequestToken() {
            string token = CryptographyServices.ToRequestToken(null, this);
            return token;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        internal LicenseRequest() {
            try {
                licenseRequestedDateTime = DateTime.UtcNow.ToUniversalTime();
                licenseRequestID = Guid.NewGuid();
                version = ((AssemblyFileVersionAttribute)Assembly.GetExecutingAssembly().
                    GetCustomAttributes(typeof(AssemblyFileVersionAttribute), true).
                    Single()).Version;

                RijndaelManaged rijndael = new RijndaelManaged();
                rijndael.GenerateIV();
                rijndael.GenerateKey();
                rijndaelIV = rijndael.IV;
                rijndaelKey = rijndael.Key;

                LoadWindowsIdentityProfile();
                LoadBIOSProfile();
                LoadProcessorProfile();
                LoadNetworkAdapterProfile();
            }
            catch (Exception cause) {
                LicenseRequestException exception = new LicenseRequestException(
                    ("An unexpected exception occurred while trying to generate the license request: " + cause.Message),
                    cause);

                throw exception;
            }
        }

        /// <summary>
        /// Load the profile for the requesting user's Windows identity.
        /// </summary>
        private void LoadWindowsIdentityProfile() {
            using (WindowsIdentity windowsIdentity = WindowsIdentity.GetCurrent()) {
                userName = windowsIdentity.Name;
                userSID = windowsIdentity.User.ToString();
            }

            if (!string.IsNullOrEmpty(License.UserName)) {
                userName = License.UserName;
            }
        }

        /// <summary>
        /// Load the BIOS serial number(s).
        /// </summary>
        private void LoadBIOSProfile() {
            biosSerialNumber = LoadProfiles("Win32_BIOS", "SerialNumber");
        }

        /// <summary>
        /// Load the CPU ID(s).
        /// </summary>
        private void LoadProcessorProfile() {
            cpuID = LoadProfiles("Win32_Processor", "DeviceId", "Name", "ProcessorId");
        }

        /// <summary>
        /// Load the MAC addresses.
        /// </summary>
        private void LoadNetworkAdapterProfile() {
            macAddresses = LoadProfiles("Win32_NetworkAdapter", "MACAddress");
        }

        /// <summary>
        /// Create profiles for the specified management object(s).
        /// </summary>
        /// <param name="table">the management object table name</param>
        /// <param name="keys">the keys to pull from the management objects</param>
        /// <returns>the profiles</returns>
        private string LoadProfiles(string table, params string[] keys) {
            List<string> profiles = new List<string>();

            try {
                using (ManagementObjectSearcher query = new ManagementObjectSearcher("SELECT * FROM " + table)) {
                    using (ManagementObjectCollection results = query.Get()) {
                        IEnumerator iresult = results.GetEnumerator();
                        for (;;) {
                            try {
                                bool hasNext = iresult.MoveNext();
                                if (!hasNext) {
                                    break;
                                }
                            }
                            catch {
                                break;
                            }

                            ManagementObject result = (ManagementObject)iresult.Current;

                            using (result) {
                                StringBuilder builder = new StringBuilder();
                                bool valid = true;
                                bool first = true;
                                foreach (string key in keys) {
                                    string value = Convert.ToString(result.GetPropertyValue(key));
                                    if (string.IsNullOrWhiteSpace(value)) {
                                        valid = false;

                                        break;
                                    }
                                    else if (first) {
                                        first = false;

                                        builder.Append(value);
                                    }
                                    else {
                                        builder.Append(";");
                                        builder.Append(key.ToLower());
                                        builder.Append("=");
                                        builder.Append(value);
                                    }
                                }

                                if (valid) {
                                    string profile = builder.ToString();
                                    profiles.Add(profile);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ignored) {
                Console.WriteLine(ignored);
            }

            StringBuilder buffer = new StringBuilder();
            string separator = string.Empty;
            foreach (string profile in profiles.OrderBy(it => it)) {
                buffer.Append(separator);
                separator = ",";

                buffer.Append(profile);
            }

            string joined = buffer.ToString();

            return joined;
        }

        /// <summary>
        /// Get a response token for the given object.
        /// </summary>
        /// <param name="marker">the marker</param>
        /// <param name="obj">the object</param>
        /// <returns>the response token for the given object</returns>
        public string GetJacketResponseToken(string marker, object obj) {
            string token;
            byte[] encrypted = CryptographyServices.EncryptObject(GetRijndaelManaged(), obj);
            token = CryptographyServices.GetJacketToken(encrypted);

            return token;
        }

        private RijndaelManaged GetRijndaelManaged() {
            RijndaelManaged rijndael = new RijndaelManaged();
            rijndael.IV = rijndaelIV;
            rijndael.Key = rijndaelKey;

            return rijndael;
        }

        /// <summary>
        /// Try to get the specified response object.
        /// </summary>
        /// <param name="marker">the marker</param>
        /// <param name="text"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool TryGetJacketResponseToken<T>(string text, out string matched, out T obj) {
            bool found;
            try {
                byte[] encrypted;
                found = CryptographyServices.TryGetJacketToken(text, out matched, out encrypted);
                if (!found) {
                    matched = null;
                    obj = default(T);
                }
                else {
                    obj = CryptographyServices.DecryptObject<T>(GetRijndaelManaged(), encrypted);
                }
            }
            catch {
                found = false;
                matched = null;
                obj = default(T);
            }

            return found;
        }

        #region Interface Implementations

        #region Interface Implementation :: ZydecoSoftwareLLC.Licensing.IBinarySerializable

        /// <summary>
        /// Constructor.
        /// </summary>
        private LicenseRequest(BinaryReader reader) {
            // version
            reader.ReadInt32();

            // id
            byte[] licenseRequestIDBytes = reader.ReadBytes();
            licenseRequestID = new Guid(licenseRequestIDBytes);

            // licenseRequestedDateTime
            Int64 licenseRequestedDateTimeTicks = reader.ReadInt64();
            licenseRequestedDateTime = new DateTime(licenseRequestedDateTimeTicks);

            // rijndael
            byte[] iv = reader.ReadBytes();
            rijndaelIV = iv;
            byte[] key = reader.ReadBytes();
            rijndaelKey = key;

            // userName
            userName = reader.ReadString();

            // userSID
            userSID = reader.ReadString();

            // biosSerialNumber
            biosSerialNumber = reader.ReadString();

            // cpuID
            cpuID = reader.ReadString();

            // macAddresses
            macAddresses = reader.ReadString();

            // version
            version = reader.ReadString();
        }

        public void WriteTo(BinaryWriter writer) {
            // version
            writer.Write((Int32)0);

            // licenseRequestID
            byte[] licenseRequestIDBytes = licenseRequestID.ToByteArray();
            writer.WriteBytes(licenseRequestIDBytes);

            // licenseRequestedDateTime
            Int64 licenseRequestedDateTimeTicks = licenseRequestedDateTime.Ticks;
            writer.Write(licenseRequestedDateTimeTicks);

            // rijndael
            byte[] iv = rijndaelIV;
            writer.WriteBytes(iv);
            byte[] key = rijndaelKey;
            writer.WriteBytes(key);

            // userName
            writer.Write(userName);

            // userSID
            writer.Write(userSID);

            // biosSerialNumber
            writer.Write(biosSerialNumber);

            // cpuID
            writer.Write(cpuID);

            // macAddresses
            writer.Write(macAddresses);

            // version
            writer.Write(version);
        }

        #endregion

        #endregion

        private string GetVersionPart(int index) {
                string value;
                if (string.IsNullOrWhiteSpace(version)) {
                    value = null;
                }
                else {
                    string[] parts = version.Split('.');
                    if ((parts == null) || (parts.Length <= index) || string.IsNullOrWhiteSpace(parts[index])) {
                        value = null;
                    }
                    else {
                        value = parts[index];
                    }
                }

                return value;
        }

        internal string TryEnsureIsLicensed(bool server, MethodBase method) {
            if (licenseCertificate == null) {
                return "The license is unauthorized.";
            }

            if (licenseCertificate.IsServerCertificate != server) {
                if (server) {
                    return "The license was issued for a workstation.";
                }
                else {
                    return "The license was issued for a server.";
                }
            }

            if (actual == null) {
                actual = new LicenseRequest();
            }

            if (licenseCertificate.IsUserNamedLocked && !string.Equals(userName, actual.userName, StringComparison.OrdinalIgnoreCase)) {
                License.Log("The license was issued for a different user (failure code LF01); expected {0} but found {1}.", userName, actual.userName);

                return "The license was issued for a different user (failure code LF01).";
            }

            if (licenseCertificate.IsUserSIDLocked && !string.Equals(userSID, actual.userSID, StringComparison.OrdinalIgnoreCase)) {
                License.Log("The license was issued for a different user (failure code LF03); expected {0} but found {1}.", userSID, actual.userSID);

                return "The license was issued for a different user (failure code LF03).";
            }

            if (licenseCertificate.IsBIOSSerialNumberLocked && !string.Equals(biosSerialNumber, actual.biosSerialNumber, StringComparison.OrdinalIgnoreCase)) {
                License.Log("The license was issued for a different computer (failure code LF05); expected {0} but found {1}.", biosSerialNumber, actual.biosSerialNumber);

                return "The license was issued for a different computer (failure code LF05).";
            }

            if (licenseCertificate.IsCPUIDLocked && !string.Equals(cpuID, actual.cpuID, StringComparison.OrdinalIgnoreCase)) {
                License.Log("The license was issued for a different computer (failure code LF07); expected {0} but found {1}.", cpuID, actual.cpuID);

                return "The license was issued for a different computer (failure code LF07).";
            }

            if (licenseCertificate.IsMACAddressesLocked && !MACAddressesMatch(macAddresses, actual.macAddresses)) {
                License.Log("The license was issued for a different computer (failure code LF09); expected one of {0} but found {1}.", macAddresses, actual.macAddresses);

                return "The license was issued for a different computer (failure code LF09).";
            }

            string majorVersion = GetVersionPart(0);
            string minorVersion = GetVersionPart(1);
            string revision = GetVersionPart(2);
            string build = GetVersionPart(3);
            string actualMajorVersion = actual.GetVersionPart(0);
            string actualMinorVersion = actual.GetVersionPart(1);
            string actualRevision = actual.GetVersionPart(2);
            string actualBuild = actual.GetVersionPart(3);

            if (licenseCertificate.IsMajorVersionLocked && !string.Equals(majorVersion, actualMajorVersion, StringComparison.OrdinalIgnoreCase)) {
                License.Log("The license was issued for a different computer (failure code LF11); expected one of {0} but found {1}.", majorVersion, actualMajorVersion);

                return "The license was issued for a different computer (failure code LF11).";
            }

            if (licenseCertificate.IsMinorVersionLocked && !string.Equals(minorVersion, actualMinorVersion, StringComparison.OrdinalIgnoreCase)) {
                License.Log("The license was issued for a different computer (failure code LF13); expected one of {0} but found {1}.", minorVersion, actualMinorVersion);

                return "The license was issued for a different computer (failure code LF13).";
            }

            if (licenseCertificate.IsRevisionLocked && !string.Equals(revision, actualRevision, StringComparison.OrdinalIgnoreCase)) {
                License.Log("The license was issued for a different computer (failure code LF15); expected one of {0} but found {1}.", revision, actualRevision);

                return "The license was issued for a different computer (failure code LF15).";
            }

            if (licenseCertificate.IsBuildLocked && !string.Equals(build, actualBuild, StringComparison.OrdinalIgnoreCase)) {
                License.Log("The license was issued for a different computer (failure code LF17); expected one of {0} but found {1}.", build, actualBuild);

                return "The license was issued for a different computer (failure code LF17).";
            }

            DateTime now = DateTime.UtcNow;
            now = new DateTime(now.Ticks - (now.Ticks % TimeSpan.TicksPerDay));
            if (licenseCertificate.IsExpiring && (now >= licenseCertificate.ExpiresDateTime)) {
                License.Log("The license expired on {0:dddd, MMMM d, yyyy} (failure code LF19).", LicenseCertificate.ExpiresDateTime);

                return string.Format("The license expired on {0:dddd, MMMM d, yyyy} (failure code LF19).", LicenseCertificate.ExpiresDateTime);
            }

            return null;
        }

        public override bool Equals(object obj) {
            if (obj == this) {
                return true;
            }

            if (!(obj is LicenseRequest)) {
                return false;
            }

            LicenseRequest actual = (LicenseRequest)obj;
                if (!string.Equals(userName, actual.userName, StringComparison.OrdinalIgnoreCase)) {
                return false;
            }

            if (!string.Equals(userSID, actual.userSID, StringComparison.OrdinalIgnoreCase)) {
                return false;
            }

            if (!string.Equals(cpuID, actual.cpuID, StringComparison.OrdinalIgnoreCase)) {
                return false;
            }

            if (!MACAddressesMatch(macAddresses, actual.macAddresses)) {
                return false;
            }

            string majorVersion = GetVersionPart(0);
            string minorVersion = GetVersionPart(1);
            string revision = GetVersionPart(2);
            string build = GetVersionPart(3);
            string actualMajorVersion = actual.GetVersionPart(0);
            string actualMinorVersion = actual.GetVersionPart(1);
            string actualRevision = actual.GetVersionPart(2);
            string actualBuild = actual.GetVersionPart(3);

            if (!string.Equals(majorVersion, actualMajorVersion, StringComparison.OrdinalIgnoreCase)) {
                return false;
            }

            if (!string.Equals(minorVersion, actualMinorVersion, StringComparison.OrdinalIgnoreCase)) {
                return false;
            }

            if (!string.Equals(revision, actualRevision, StringComparison.OrdinalIgnoreCase)) {
                return false;
            }

            if (!string.Equals(build, actualBuild, StringComparison.OrdinalIgnoreCase)) {
                return false;
            }

            return true;
        }

        public override int GetHashCode() {
            string str = ToString();
            int hashCode = str.GetHashCode();
            return hashCode;
        }

        private bool MACAddressesMatch(string expected, string actual) {
            bool matched = false;
            HashSet<string> expecteds = new HashSet<string>();
            if (expected != null) {
                foreach (string address in expected.Split(',')) {
                    string f = address.ToLowerInvariant().Trim();
                    expecteds.Add(f);
                }
            }

            if (actual != null) {
                foreach (string address in actual.Split(',')) {
                    string f = address.ToLowerInvariant().Trim();
                    if (expecteds.Contains(f)) {
                        matched = true;

                        break;
                    }
                }
            }

            return matched;
        }

        private static LicenseRequest actual;

        internal void GetBriefInfo(ref DateTime latestExpirationDateTime) {
            DateTime now = DateTime.UtcNow;
            now = new DateTime(now.Ticks - ((int)(now.Ticks / TimeSpan.TicksPerDay) * TimeSpan.TicksPerDay));
            if ((licenseCertificate != null) && licenseCertificate.IsExpiring) {
                if ((latestExpirationDateTime.Ticks == 0) || (licenseCertificate.ExpiresDateTime > latestExpirationDateTime)) {
                    latestExpirationDateTime = licenseCertificate.ExpiresDateTime;
                }
            }
        }

        public override string ToString() {
            string value;
            StringBuilder buffer = new StringBuilder();
            
            buffer.Append("LicenseRequestID: ");
            buffer.Append(licenseRequestID);
            buffer.Append("\r\n");

            buffer.Append("LicenseRequestedDateTime: ");
            buffer.Append(licenseRequestedDateTime);
            buffer.Append("\r\n");

            if (!string.IsNullOrWhiteSpace(userName)) {
                buffer.Append("UserName: ");
                buffer.Append(userName);
                buffer.Append("\r\n");
            }

            if (!string.IsNullOrWhiteSpace(userSID)) {
                buffer.Append("UserSID: ");
                buffer.Append(userSID);
                buffer.Append("\r\n");
            }

            if (!string.IsNullOrWhiteSpace(biosSerialNumber)) {
                buffer.Append("BIOSSerialNumber: ");
                buffer.Append(biosSerialNumber);
                buffer.Append("\r\n");
            }

            if (!string.IsNullOrWhiteSpace(cpuID)) {
                buffer.Append("CPUID: ");
                buffer.Append(cpuID);
                buffer.Append("\r\n");
            }

            if (!string.IsNullOrWhiteSpace(macAddresses)) {
                buffer.Append("MACAddresses: ");
                buffer.Append(macAddresses);
                buffer.Append("\r\n");
            }

            if (!string.IsNullOrWhiteSpace(version)) {
                buffer.Append("Version: ");
                buffer.Append(version);
                buffer.Append("\r\n");
            }

            value = buffer.ToString();

            return value;
        }
    }
}
