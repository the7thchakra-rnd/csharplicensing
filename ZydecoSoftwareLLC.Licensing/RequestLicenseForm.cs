﻿/**
 * RequestLicenseForm.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using ZydecoSoftwareLLC.Licensing.Properties;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace ZydecoSoftwareLLC.Licensing {
    public partial class RequestLicenseForm: ClipboardAwareForm {
        private bool initialized;
        private LicenseCertificate licenseCertificate;
        private string licenseCertificateText;
        private string reasonText;

        public string Reason {
            get { return reasonText; }
            set {
                reasonText = value;
                if (reason != null) {
                    reason.Text = reasonText;
                }
            }
        }

        public RequestLicenseForm() {
            InitializeComponent();
            initialized = true;

            CenterToScreen();

            mailtoAddress.Text = Resources.MAILTO_RECIPIENT;

            DrawClipboard();
        }

        protected override void DrawClipboard() {
            if (initialized) {
                string text = Clipboard.GetText();

                // Try to get the license from the clipboard:
                LicenseCertificate licenseCertificate;
                string licenseCertificateText;
                bool changed;
                try {
                    License.Instance.TryGetLicenseCertificate(text, out licenseCertificateText, out licenseCertificate);
                    changed = ((licenseCertificate != null) && !string.Equals(this.licenseCertificateText, licenseCertificateText));
                }
                catch {
                    licenseCertificate = null;
                    licenseCertificateText = null;
                    changed = false;
                }

                if (licenseCertificate == null) {
                    if (this.licenseCertificate == null) {
                        // clipboardContents.Text = string.Empty;

                        this.licenseCertificate = null;
                        this.licenseCertificateText = null;
                    }
                }
                else if (changed) {
                    clipboardContents.Text = licenseCertificateText;

                    this.licenseCertificate = licenseCertificate;
                    this.licenseCertificateText = licenseCertificateText;
                }

                applyLicense.Enabled = (this.licenseCertificate != null);

                if (this.licenseCertificate == null) {
                    licenseDetails.Text = string.Empty;
                }
                else {
                    string details;
                    StringBuilder detailsBuilder = new StringBuilder();
                    detailsBuilder.Append(string.Format("Issued {0:dddd, MMMM d, yyyy}", this.licenseCertificate.IssuedDateTime));
                    if (this.licenseCertificate.IsExpiring) {
                        detailsBuilder.Append(string.Format("; expires {0:dddd, MMMM d, yyyy}", this.licenseCertificate.ExpiresDateTime));
                    }
                    details = detailsBuilder.ToString();

                    licenseDetails.Text = details;
                }
            }
        }

        private void RequestLicenseForm_Load(object sender, EventArgs eventArgs) {
        }

        private void copyLicenseRequestToClipboardAction_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            Cursor savedCursor = Cursor;
            Cursor = Cursors.WaitCursor;
            try {

                // Put the license request in the clipboard:
                LicenseRequest request = License.Instance.NewLicenseRequest();
                string token = CryptographyServices.ToRequestToken("LicenseRequest", request);

                Clipboard.SetText(token);
            }
            catch (Exception cause) {
                ReportFailureForm.ReportFailure(cause);
            }
            finally {
                Cursor = savedCursor;
            }
        }

        private void requestLicense_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            Cursor savedCursor = Cursor;
            Cursor = Cursors.WaitCursor;
            try {
                // Put the license request in the clipboard:
                LicenseRequest request = License.Instance.NewLicenseRequest();
                string token = CryptographyServices.ToRequestToken("LicenseRequest", request);

                String uri;
                StringBuilder uriBuilder = new StringBuilder();
                uriBuilder.Append(Uri.UriSchemeMailto);
                uriBuilder.Append(":");
                uriBuilder.Append(Resources.MAILTO_RECIPIENT);
                uriBuilder.Append("?");
                uriBuilder.Append("subject=");
                uriBuilder.Append(Uri.EscapeUriString(Resources.MAILTO_SUBJECT));
                uriBuilder.Append("&body=");
                uriBuilder.Append(Uri.EscapeUriString(token));
                uri = uriBuilder.ToString();

                Process.Start(uri);
            }
            catch (Exception cause) {
                ReportFailureForm.ReportFailure(cause);
            }
            finally {
                Cursor = savedCursor;
            }
        }

        private void sendEmail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            Cursor savedCursor = Cursor;
            Cursor = Cursors.WaitCursor;
            try {
                String uri;
                StringBuilder uriBuilder = new StringBuilder();
                uriBuilder.Append(Uri.UriSchemeMailto);
                uriBuilder.Append(":");
                uriBuilder.Append(Resources.MAILTO_RECIPIENT);
                uriBuilder.Append("?");
                uriBuilder.Append("subject=");
                uriBuilder.Append(Uri.EscapeUriString(Resources.MAILTO_SUBJECT));
                uri = uriBuilder.ToString();

                Process.Start(uri);
            }
            catch (Exception cause) {
                ReportFailureForm.ReportFailure(cause);
            }
            finally {
                Cursor = savedCursor;
            }
        }

        private void applyLicense_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            if (licenseCertificate != null) {
                Cursor savedCursor = Cursor;
                Cursor = Cursors.WaitCursor;
                try {
                    License.Instance.ApplyLicense(licenseCertificate);
                }
                catch (Exception cause) {
                    ReportFailureForm.ReportFailure(cause);
                }
                finally {
                    Cursor = savedCursor;
                    
                    Close();
                }
            }
        }

        private void ok_Click(object sender, EventArgs eventArgs) {
            Close();
        }

        private void clipboardContents_TextChanged(object sender, EventArgs e) {
            if (initialized) {
                string text = clipboardContents.Text;

                // Try to get the license from the clipboard:
                LicenseCertificate licenseCertificate;
                string licenseCertificateText;
                bool changed;
                try {
                    License.Instance.TryGetLicenseCertificate(text, out licenseCertificateText, out licenseCertificate);
                    changed = ((licenseCertificate != null) && !string.Equals(this.licenseCertificateText, licenseCertificateText));
                }
                catch {
                    licenseCertificate = null;
                    licenseCertificateText = null;
                    changed = false;
                }

                if (licenseCertificate == null) {
                    if (this.licenseCertificate == null) {
                        // clipboardContents.Text = string.Empty;

                        this.licenseCertificate = null;
                        this.licenseCertificateText = null;
                    }
                }
                else if (changed) {
                    clipboardContents.Text = licenseCertificateText;

                    this.licenseCertificate = licenseCertificate;
                    this.licenseCertificateText = licenseCertificateText;
                }

                applyLicense.Enabled = (this.licenseCertificate != null);

                if (this.licenseCertificate == null) {
                    licenseDetails.Text = string.Empty;
                }
                else {
                    string details;
                    StringBuilder detailsBuilder = new StringBuilder();
                    detailsBuilder.Append(string.Format("Issued {0:dddd, MMMM d, yyyy}", this.licenseCertificate.IssuedDateTime));
                    if (this.licenseCertificate.IsExpiring) {
                        detailsBuilder.Append(string.Format("; expires {0:dddd, MMMM d, yyyy}", this.licenseCertificate.ExpiresDateTime));
                    }
                    details = detailsBuilder.ToString();

                    licenseDetails.Text = details;
                }
            }
        }
    }
}
