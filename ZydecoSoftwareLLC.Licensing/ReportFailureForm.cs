﻿/**
 * ReportFailureForm.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using ZydecoSoftwareLLC.Licensing.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ZydecoSoftwareLLC.Licensing {
    public partial class ReportFailureForm: Form {
        private string message;

        public static void ReportFailure(Exception exception) {
            using (ReportFailureForm reportFailureForm = new ReportFailureForm()) {
                if (exception == null) {
                    reportFailureForm.message = string.Empty;
                }
                else {
                    reportFailureForm.message = exception.ToString();
                }

                try {
                    Clipboard.SetText(reportFailureForm.message);
                }
                catch {
                }

                reportFailureForm.exceptionDetails.Text = reportFailureForm.message;
                reportFailureForm.CenterToScreen();
                reportFailureForm.ShowDialog();
            }
        }

        public ReportFailureForm() {
            InitializeComponent();

            mailtoAddress.Text = Resources.MAILTO_RECIPIENT;
        }

        private void sendEmail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            Cursor savedCursor = Cursor;
            Cursor = Cursors.WaitCursor;
            try {
                String uri;
                StringBuilder uriBuilder = new StringBuilder();
                uriBuilder.Append(Uri.UriSchemeMailto);
                uriBuilder.Append(":");
                uriBuilder.Append(Resources.MAILTO_RECIPIENT);
                uriBuilder.Append("?");
                uriBuilder.Append("subject=");
                uriBuilder.Append(Uri.EscapeUriString(Resources.MAILTO_SUBJECT));
                uriBuilder.Append("&body=");
                uriBuilder.Append(Uri.EscapeUriString(message));
                uri = uriBuilder.ToString();

                Process.Start(uri);
            }
            catch (Exception cause) {
                string message = cause.ToString();

                MessageBox.Show(message);
            }
            finally {
                Cursor = savedCursor;
            }
        }

        private void ReportFailureForm_Load(object sender, EventArgs eventArgs) {
        }

        private void ok_Click(object sender, EventArgs eventArgs) {
            Close();
        }
    }
}
