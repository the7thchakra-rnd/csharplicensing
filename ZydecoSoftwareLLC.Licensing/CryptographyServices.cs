﻿/**
 * CryptographyServices.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace ZydecoSoftwareLLC.Licensing {

    /// <summary>
    /// Cryptography services.
    /// </summary>
    [ComVisible(false)]
    public static class CryptographyServices {
        internal static string ToRequestToken(string marker, object obj) {
            string value;
            using (MemoryStream buffer = new MemoryStream()) {
                if (obj is IBinarySerializable) {
                    using (BinaryWriter writer = new BinaryWriter(buffer)) {
                        ((IBinarySerializable)obj).WriteTo(writer);
                    }
                }
                else {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.AssemblyFormat = FormatterAssemblyStyle.Simple;
                    formatter.Serialize(buffer, obj);
                }

                byte[] bytes = buffer.ToArray();
                byte[] encrypted = PublicKeyEncrypt(bytes);

                value = GetBlockToken(marker, encrypted);
            }

            return value;
        }

        internal static string GetBlockToken(string marker, byte[] bytes) {
            string token = Convert.ToBase64String(bytes);

            StringBuilder blockTokenBuilder = new StringBuilder();

            if (marker != null) {
                blockTokenBuilder.Append("<");
                blockTokenBuilder.Append(marker);
                blockTokenBuilder.Append(">\r\n");
            }

            int length = token.Length;
            for (int start = 0, end; start < length; start = end) {
                end = Math.Min(length, (start + 78));
                blockTokenBuilder.Append(token.Substring(start, (end - start)));
                blockTokenBuilder.Append("\r\n");
            }

            if (marker != null) {
                blockTokenBuilder.Append("</");
                blockTokenBuilder.Append(marker);
                blockTokenBuilder.Append(">\r\n");
            }

            string blockToken = blockTokenBuilder.ToString();

            return blockToken;
        }

        internal static string GetJacketToken(byte[] bytes) {
            string token;
            StringBuilder buffer = new StringBuilder();
            int bits = 8;
            int v = (int)bytes[0] & 255;
            int n = bytes.Length;
            int i = 1;
            int counter = 0;
            while ((i < n) || (bits > 0) || (counter == 0) || ((counter % 4) != 0)) {
                if ((counter > 0) && ((counter % 4) == 0)) {
                    buffer.Append("-");
                }
                if (bits < 0) {
                    buffer.Append('9');
                }
                else {
                    buffer.Append(EncodeJacket(v));
                }
                counter += 1;
                v >>= 5;
                bits -= 5;
                if ((bits < 5) && (i < n)){
                    v |= ((int)bytes[i++] & 255) << bits;
                    bits += 8;
                }
            }
            token = buffer.ToString();

            return token;
        }

        private static char EncodeJacket(int v) {
            v = (v & 31);
            if (v < 26) {
                return (char)((int)'A' + v);
            }
            return (char)((int)'0' + (v - 26));
        }

        public static string ToRawToken(object obj) {
            string value;
            using (MemoryStream buffer = new MemoryStream()) {
                using (CryptoStream encryptor = GetEncryptStream(buffer, CryptoStreamMode.Write)) {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.AssemblyFormat = FormatterAssemblyStyle.Simple;
                    formatter.Serialize(encryptor, obj);
                }

                byte[] bytes = buffer.ToArray();
                value = Convert.ToBase64String(bytes);
            }

            return value;
        }

        public static string ToToken(object obj) {
            string value;
            using (MemoryStream buffer = new MemoryStream()) {
                using (CryptoStream encryptor = GetEncryptStream(buffer, CryptoStreamMode.Write)) {
                    if (obj is IBinarySerializable) {
                        using (BinaryWriter writer = new BinaryWriter(encryptor)) {
                            ((IBinarySerializable)obj).WriteTo(writer);
                        }
                    }
                    else {
                        BinaryFormatter formatter = new BinaryFormatter();
                        formatter.AssemblyFormat = FormatterAssemblyStyle.Simple;
                        formatter.Serialize(encryptor, obj);
                    }
                }

                byte[] bytes = buffer.ToArray();
                value = Convert.ToBase64String(bytes);
            }

            return value;
        }

        /// <summary>
        /// Try to extract a response object as a token (base-64 encoded byte sequence
        /// embedded in XML) from the given text.
        /// </summary>
        /// <param name="marker">the token marker (tag name)</param>
        /// <param name="text">the text</param>
        /// <param name="responseObject">the response object</param>
        public static bool TryGetResponseObject<T>(string marker, string text, out string matched, out T responseObject) {
            bool found;

            string token = GetToken(marker, text, out matched);
            if (token == null) {
                found = false;

                responseObject = default(T);
            }
            else {
                found = true;

                byte[] encrypted = Convert.FromBase64String(token);
                byte[] bytes = encrypted; // TODO:
                responseObject = (T)ToObject<T>(bytes);
            }

            return found;
        }

        public static bool TryGetBlockToken(string marker, string text, out string matched, out byte[] bytes) {
            bool found;
            try {
                string token = GetToken(marker, text, out matched);
                if (string.IsNullOrEmpty(token)) {
                    found = false;
                    bytes = null;
                }
                else {
                    found = true;
                    bytes = Convert.FromBase64String(token);
                }
            }
            catch {
                found = false;
                matched = null;
                bytes = null;
            }

            return found;
        }

        public static bool TryGetJacketToken(string text, out string matched, out byte[] bytes) {
            bool found;
            try {
                Regex regex = new Regex("(([A-Z0-9]{4})(-[A-Z0-9]{4})*)");
                Match match = regex.Match(text);
                if ((match == null) || !match.Success) {
                    found = false;
                    matched = null;
                    bytes = null;
                }
                else {
                    found = true;
                    matched = match.Groups[0].ToString();
                    List<byte> tmp = new List<byte>();
                    char[] chars = matched.Replace("-", "").ToCharArray();
                    int bits = 0;
                    int v = 0;
                    int n = chars.Length;
                    int i = 0;
                    while (i < n) {
                        while ((i < n) && (bits < 8)) {
                            char ch = chars[i];
                            if (ch == '9') {
                                i = n;
                            }
                            else if ((ch >= 'A') && (ch <= 'Z')) {
                                int t = (ch - 'A');
                                v |= (t << bits);
                                bits += 5;
                            }
                            else {
                                int t = ((ch - '0') + 26);
                                v |= (t << bits);
                                bits += 5;
                            }

                            i += 1;
                        }
                        while (bits >= 8) {
                            tmp.Add((byte)(v & 255));
                            v >>= 8;
                            bits -= 8;
                        }
                    }
                    bytes = tmp.ToArray();
                }
            }
            catch {
                found = false;
                matched = null;
                bytes = null;
            }

            return found;
        }

        /// <summary>
        /// Extract a token (base-64 encoded byte sequence embedded in XML)
        /// from the given text.
        /// </summary>
        /// <param name="marker">the token marker (tag name)</param>
        /// <param name="text">the text</param>
        /// <returns>
        /// the token
        /// </returns>
        public static string GetToken(string marker, string text) {
            string matched;
            return GetToken(marker, text, out matched);
        }

        public static string GetToken(string marker, string text, out string matched) {
            matched = null;

            string token;
            if (text == null) {
                token = null;
            }
            else {
                string startTag = ("<" + marker);
                string endTag = ("</" + marker);
                int start = text.IndexOf(startTag);
                if (start < 0) {
                    token = null;
                }
                else {
                    int end = text.IndexOf(endTag, start);
                    if (end < 0) {
                        token = null;
                    }
                    else {
                        end = text.IndexOf(">", end);
                        if (end < 0) {
                            token = null;
                        }
                        else {
                            string xml = text.Substring(start, (end - start + 1));
                            matched = xml;

                            XmlDocument document = new XmlDocument();
                            document.LoadXml(xml);

                            token = document.DocumentElement.InnerText;
                        }
                    }
                }
            }

            return token;
        }

        public static T ToObject<T>(byte[] bytes) {
            object obj;

            using (MemoryStream buffer = new MemoryStream(bytes)) {
                if (typeof(IBinarySerializable).IsAssignableFrom(typeof(T))) {
                    using (BinaryReader reader = new BinaryReader(buffer)) {
                        ConstructorInfo constructor = typeof(T).GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] {
                            typeof(BinaryReader)
                        }, null);

                        obj = constructor.Invoke(new object[] {
                            reader
                        });
                    }
                }
                else {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.AssemblyFormat = FormatterAssemblyStyle.Simple;
                    obj = formatter.Deserialize(buffer);
                }
            }

            return (T)obj;
        }

        public static T GetFromRawToken<T>(string token) {
            T value;
            if (token == null) {
                value = default(T);
            }
            else {
                byte[] bytes = Convert.FromBase64String(token);
                using (MemoryStream buffer = new MemoryStream()) {
                    using (CryptoStream decryptor = GetDecryptStream(buffer, CryptoStreamMode.Write)) {
                        decryptor.Write(bytes, 0, bytes.Length);
                    }

                    bytes = buffer.ToArray();
                }

                using (MemoryStream buffer = new MemoryStream(bytes)) {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.AssemblyFormat = FormatterAssemblyStyle.Simple;
                    value = (T)formatter.Deserialize(buffer);
                }
            }

            return value;
        }

        public static T GetFromToken<T>(string token) {
            T value;
            if (token == null) {
                value = default(T);
            }
            else {
                byte[] bytes = Convert.FromBase64String(token);
                using (MemoryStream buffer = new MemoryStream(bytes)) {
                    using (CryptoStream decryptor = GetDecryptStream(buffer, CryptoStreamMode.Read)) {
                        if (typeof(IBinarySerializable).IsAssignableFrom(typeof(T))) {
                            using (BinaryReader reader = new BinaryReader(buffer)) {
                                ConstructorInfo constructor = typeof(T).GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] {
                                    typeof(BinaryReader)
                                }, null);

                                value = (T)constructor.Invoke(new object[] {
                                    reader
                                });
                            }
                        }
                        else {
                            BinaryFormatter formatter = new BinaryFormatter();
                            formatter.AssemblyFormat = FormatterAssemblyStyle.Simple;
                            value = (T)formatter.Deserialize(decryptor);
                        }
                    }
                }
            }

            return value;
        }

        public static byte[] PublicKeyEncrypt(byte[] bytes) {
            byte[] encrypted = ZydecoSoftwareLLC.Licensing.PublicKeyEncrypt.Encrypt(bytes);

            return encrypted;
        }

        public static byte[] Encrypt(byte[] password, byte[] bytes) {
            byte[] cypher;

            PasswordDeriveBytes keyGenerator = new PasswordDeriveBytes(password, password);
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] iv = des.IV;
            iv = new byte[iv.Length];
            byte[] key = keyGenerator.CryptDeriveKey("DES", "SHA1", des.KeySize, iv);
            des.Key = key;
            des.IV = iv;
            using (MemoryStream buffer = new MemoryStream()) {
                using (CryptoStream encoder = new CryptoStream(buffer, des.CreateEncryptor(), CryptoStreamMode.Write)) {
                    encoder.Write(bytes, 0, bytes.Length);
                }

                cypher = buffer.ToArray();
            }

            return cypher;
        }

        private static ConstructorInfo[] privateKeyDecryptConstructor = new ConstructorInfo[1];

        public static byte[] Decrypt(byte[] password, byte[] cypher) {
            byte[] bytes;

            PasswordDeriveBytes keyGenerator = new PasswordDeriveBytes(password, password);
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] iv = des.IV;
            iv = new byte[iv.Length];
            byte[] key = keyGenerator.CryptDeriveKey("DES", "SHA1", des.KeySize, iv);
            des.Key = key;
            des.IV = iv;
            using (MemoryStream buffer = new MemoryStream()) {
                using (CryptoStream encoder = new CryptoStream(buffer, des.CreateDecryptor(), CryptoStreamMode.Write)) {
                    encoder.Write(cypher, 0, cypher.Length);
                }

                bytes = buffer.ToArray();
            }

            return bytes;
        }
        private static CryptoStream GetDecryptStream(Stream stream, CryptoStreamMode mode) {
            RijndaelManaged rijndael = GetRijndaelManaged();
            ICryptoTransform transform = rijndael.CreateDecryptor();
            CryptoStream value = new CryptoStream(stream, transform, mode);
            return value;
        }

        private static CryptoStream GetEncryptStream(Stream stream, CryptoStreamMode mode) {
            RijndaelManaged rijndael = GetRijndaelManaged();
            ICryptoTransform transform = rijndael.CreateEncryptor();
            CryptoStream value = new CryptoStream(stream, transform, mode);
            return value;
        }

        private static RijndaelManaged GetRijndaelManaged() {
            RijndaelManaged rijndael = new RijndaelManaged();
            rijndael.IV = new byte[] {
                0x05, 0x67, 0x8f, 0x1b, 0x6d, 0xfa, 0x2f, 0x56, 0x6c, 0x95, 0x0a, 0xa0, 0x0f, 0x33, 0x52, 0xfb
            };
            rijndael.Key = new byte[] {
                0x4c, 0xad, 0xb6, 0x71, 0x4b, 0x89, 0xf9, 0x02, 0xa2, 0x15, 0x49, 0xc0, 0x95, 0xa2, 0xac, 0x1e, 
                0xbe, 0x66, 0x13, 0x8c, 0x5d, 0x37, 0xc9, 0x90, 0x4b, 0xa3, 0x74, 0x00, 0xcf, 0x8e, 0x8f, 0x2b
            };
            return rijndael;
        }

        internal static byte[] EncryptObject(RijndaelManaged rijndael, object value) {
            byte[] encrypted;

            using (MemoryStream buffer = new MemoryStream()) {
                using (CryptoStream encryptor = new CryptoStream(buffer, rijndael.CreateEncryptor(), CryptoStreamMode.Write)) {
                    if (value is IBinarySerializable) {
                        using (BinaryWriter writer = new BinaryWriter(encryptor)) {
                            ((IBinarySerializable)value).WriteTo(writer);
                        }
                    }
                    else {
                        BinaryFormatter formatter = new BinaryFormatter();
                        formatter.AssemblyFormat = FormatterAssemblyStyle.Simple;
                        formatter.Serialize(encryptor, value);
                    }
                }

                encrypted = buffer.ToArray();
            }

            return encrypted;
        }

        internal static byte[] Encrypt(RijndaelManaged rijndael, byte[] bytes) {
            byte[] encrypted;

            using (MemoryStream buffer = new MemoryStream()) {
                using (CryptoStream encryptor = new CryptoStream(buffer, rijndael.CreateEncryptor(), CryptoStreamMode.Write)) {
                    using (BinaryWriter writer = new BinaryWriter(encryptor)) {
                        if (bytes == null) {
                            writer.Write(false);
                        }
                        else {
                            writer.Write(true);
                            int length = bytes.Length;
                            writer.Write(length);
                            writer.Write(bytes);
                        }
                    }
                }

                encrypted = buffer.ToArray();
            }

            return encrypted;
        }

        internal static T DecryptObject<T>(RijndaelManaged rijndael, byte[] encrypted) {
            T value;

            using (MemoryStream buffer = new MemoryStream(encrypted)) {
                using (CryptoStream decryptor = new CryptoStream(buffer, rijndael.CreateDecryptor(), CryptoStreamMode.Read)) {
                    if (typeof(IBinarySerializable).IsAssignableFrom(typeof(T))) {
                        ConstructorInfo constructor = typeof(T).GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] {
                            typeof(BinaryReader)
                        }, null);

                        using (BinaryReader reader = new BinaryReader(decryptor)) {
                            value = (T)constructor.Invoke(new object[] {
                            reader
                        });
                        }
                    }
                    else {
                        BinaryFormatter formatter = new BinaryFormatter();
                        formatter.AssemblyFormat = FormatterAssemblyStyle.Simple;
                        value = (T)formatter.Deserialize(decryptor);
                    }
                }
            }

            return value;
        }

        internal static byte[] Decrypt(RijndaelManaged rijndael, byte[] encrypted) {
            byte[] bytes;

            using (MemoryStream buffer = new MemoryStream(encrypted)) {
                using (CryptoStream decryptor = new CryptoStream(buffer, rijndael.CreateDecryptor(), CryptoStreamMode.Read)) {
                    using (BinaryReader reader = new BinaryReader(decryptor)) {
                        if (!reader.ReadBoolean()) {
                            bytes = null;
                        }
                        else {
                            int length = reader.ReadInt32();
                            bytes = new byte[length];
                            reader.Read(bytes, 0, length);
                        }
                    }
                }
            }

            return bytes;
        }

    }
}
