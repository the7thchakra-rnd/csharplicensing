﻿/**
 * Program.cs
 * Copyright (C) 2011-2023 Stephanie Victoria Toupin <stephanie@toupin.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

using ZydecoSoftwareLLC.Licensing;
using ZydecoSoftwareLLC.Licensing.Management;
using System;
using System.IO;
using System.Management;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Windows.Forms;

namespace ZydecoSoftwareLLC.Build.Tools {
    public class Program {

        [STAThread]
        public static void Main(string[] arguments) {
            // TestLicensing();
            CreateLicenseCertificate(arguments[1]);
            bool v = true;
            if (v) {
                return;
            }

            string command = ((arguments.Length > 0) ? arguments[0] : null);
            if (command != null) {
                command = command.ToLower();
            }

            bool help = false;
            switch (command) {
            case "/mksecret":
                CreateRinjadelInitializer();
                break;
                    
            case "/mkpk":
                CreatePublicPrivateKeyInitializer(arguments[1]);
                break;

            case "/mklicense":
                CreateLicenseCertificate(arguments[1]);
                break;

            case "/dmo":
                DumpManagementObject(arguments[1]);
                break;  
            }

            if (help) {
                Help();
            }
        }

        private static void Help() {
        }

        /// <summary>
        /// Create initialization parameters for the embedded encryption key.
        /// </summary>
        private static void CreateRinjadelInitializer() {
            RijndaelManaged rijndael = new RijndaelManaged();
            rijndael.GenerateIV();
            rijndael.GenerateKey();

            WriteArrayInitializer("iv", rijndael.IV, Console.Out);
            WriteArrayInitializer("key", rijndael.Key, Console.Out);
        }

        private static void CreatePublicPrivateKeyInitializer(string password) {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

            byte[] csp = CryptographyServices.Encrypt(UTF8Encoding.UTF8.GetBytes(password), rsa.ExportCspBlob(true));
            WriteArrayInitializer("private", csp, Console.Out);
            WriteArrayInitializer("public", rsa.ExportCspBlob(false), Console.Out);
        }

        private static void CreateLicenseCertificate(string password) {
            LicenseManagementServices.ShowCreateLicenseCertificateForm();

            /*
            LicenseRequest request;
            string text = Clipboard.GetText();
            LicenseManagementServices.TryGetRequestObject(UTF8Encoding.UTF8.GetBytes(password), "LicenseRequest", text, out request);
             */
        }

        private static void WriteArrayInitializer(string name, byte[] bytes, TextWriter writer) {
            writer.Write("byte[] ");
            writer.Write(name);
            writer.WriteLine(" = new byte[] {");
            string separator = string.Empty;
            int n = bytes.Length;
            for (int i = 0; i < n; i += 1) {
                writer.Write(separator);
                separator = ", ";

                if ((i % 16) == 15) {
                    writer.WriteLine();
                }

                writer.Write("0x{0:x2}", bytes[i]);
            }

            writer.WriteLine();
            writer.Write(" };");
        }

        private static void DumpManagementObject(string table) {
            using (ManagementObjectSearcher query = new ManagementObjectSearcher("SELECT * FROM " + table)) {
                using (ManagementObjectCollection results = query.Get()) {
                    foreach (ManagementObject result in results) {
                        Console.WriteLine("===== ");
                        using (result) {
                            foreach (PropertyData property in result.Properties) {
                                Console.WriteLine(property.Name + ": " + property.Value);
                            }
                        }
                    }
                }
            }
        }
    }
}
